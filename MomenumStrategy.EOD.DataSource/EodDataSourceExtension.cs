﻿using Microsoft.Practices.Unity;

namespace MomenumStrategy.EOD.DataSource
{
	public class EodDataSourceExtension : UnityContainerExtension
	{
		protected override void Initialize()
		{
			Container.RegisterType<IEodDataProxy, EodDataProxy>(new ContainerControlledLifetimeManager());
			Container.RegisterType<IEodDataProvider, EodDataProvider>(new ContainerControlledLifetimeManager());
		}
	}
}
