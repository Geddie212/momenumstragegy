﻿using System;

namespace MomenumStrategy.EOD.DataSource
{
	public class InvalidSymbolCodeException : Exception
	{
		public InvalidSymbolCodeException(string exchange, string symbol, string msg) : base($"{exchange}.{symbol}:{msg}")
		{

		}
	}
}
