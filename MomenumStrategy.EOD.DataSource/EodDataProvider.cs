﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.EOD.DataSource
{
	public interface IEodDataProvider
	{
		IEnumerable<EodDataModel> GetEod(string symbol, DateTime startDate);
	}

	internal class EodDataProvider : IEodDataProvider
	{

		private readonly Dictionary<string, string> _exchanges = new Dictionary<string, string>()
		{
			{"NYSE", "NYSE"},
			{"NASDAQ", "NASDAQ"},
			{"AMEX", "AMEX"},
			{"INDEX", "INDEX"},
		};
		private readonly IEodDataProxy _eodDataProxy;

		public EodDataProvider(IEodDataProxy eodDataProxy)
		{
			_eodDataProxy = eodDataProxy;
		}

		public IEnumerable<EodDataModel> GetEod(string symbol, DateTime startDate)
		{
			var exceptions = new List<Exception>();
			foreach (var exchange in _exchanges)
			{
				try
				{
					return _eodDataProxy.GetEod(exchange.Key, symbol, startDate).Select(a => new EodDataModel
					{
						Date = a.DateTime,
						Close = Convert.ToDecimal(a.Close),
						Low = Convert.ToDecimal(a.Low),
						High = Convert.ToDecimal(a.High),
						Open = Convert.ToDecimal(a.Open),
					});
				}
				catch (InvalidSymbolCodeException e)
				{
					exceptions.Add(e);
				}
			}

			if (exceptions.Any())
			{
				throw new AggregateException("Error during getting EOD", exceptions);
			}

			return new EodDataModel[0];
		}
	}
}
