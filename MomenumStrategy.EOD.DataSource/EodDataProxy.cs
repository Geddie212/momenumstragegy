﻿using MomenumStrategy.EOD.DataSource.EodData;
using System;
using System.Collections.Generic;

namespace MomenumStrategy.EOD.DataSource
{
	public interface IEodDataProxy
	{
		IEnumerable<EXCHANGE> GetExchanges();
		IEnumerable<QUOTE> GetEod(string exchange, string symbol, DateTime startDate);
	}
	internal class EodDataProxy : IEodDataProxy
	{
		private readonly DataSoapClient _client;
		private LOGINRESPONSE _ticket2;
		private LOGINRESPONSE _ticket;

		public EodDataProxy()
		{
			//			my login is: geddie212
			//password is: FLAKIE13
			_client = new DataSoapClient("DataSoap");
			_ticket2 = _client.Login2("geddie212", "FLAKIE13", "2");
			_ticket = _client.Login("geddie212", "FLAKIE13");
			//_client.QuoteListByDate2(_ticket.Token, "NYSE", "MMM");
		}

		public IEnumerable<EXCHANGE> GetExchanges()
		{
			return _client.ExchangeList(_ticket.Token).EXCHANGES;
		}

		public IEnumerable<QUOTE> GetEod(string exchange, string symbol, DateTime startDate)
		{
			using (var client = new DataSoapClient("DataSoap"))
			{
				//var result = client.SymbolHistoryPeriodByDateRange(_ticket.Token, exchange, symbol, startDate.ToString("yyyyMMdd"),
				//	DateTime.Now.AddDays(-1).ToString("yyyyMMdd"), "d");
				var result = client.SymbolHistory(_ticket.Token, exchange, symbol, startDate.ToString("yyyyMMdd"));
				if (result.Message != "Success")
				{
					if (result.Message.Equals("Invalid symbol code", StringComparison.InvariantCultureIgnoreCase))
					{
						throw new InvalidSymbolCodeException(exchange, symbol, result.Message);
					}
					//throw new InvalidOperationException(result.Message);
				}

				return result.QUOTES;
			}
		}
	}
}
