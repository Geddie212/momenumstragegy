﻿using MomenumStrategy.Utils.Factories;
using System;
using System.IO;
using System.Net;

namespace MomenumStrategy.SAndPConstituentsSource
{
	public interface IDataOkfnClient
	{
		string LoadAsJson();
	}


	public class DataOkfnClient : IDataOkfnClient
	{
		private const string Request = "http://data.okfn.org/data/core/s-and-p-500-companies/r/constituents.json";
		private readonly ILogMessager _logMessager;

		public DataOkfnClient(ILogMessager logMessager)
		{
			_logMessager = logMessager;
		}

		public string LoadAsJson()
		{
			try
			{
				var webRequest = WebRequest.CreateHttp(Request);
				webRequest.Method = "GET";
				using (var resp = webRequest.GetResponse())
				{
					var respStream = resp.GetResponseStream();
					if (respStream != null)
						using (var sr = new StreamReader(respStream))
						{
							return sr.ReadToEnd();
						}

					return null;
				}
			}
			catch (Exception exc)
			{
				_logMessager.SendErrorEvent(exc.ToString());
				return null;
			}
		}
	}
}
