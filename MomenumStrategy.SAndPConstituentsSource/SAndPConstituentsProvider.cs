﻿using MomenumStrategy.Utils.Contituents;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.SAndPConstituentsSource
{
	public class SAndPConstituentsProvider : IIndexConstituentsSource
	{
		private readonly IDataOkfnClient _dataOkfnClient;

		public SAndPConstituentsProvider(IDataOkfnClient dataOkfnClient)
		{
			_dataOkfnClient = dataOkfnClient;
		}

		public IEnumerable<Constituent> Load()
		{
			var json = _dataOkfnClient.LoadAsJson();
		    if (json == null)
		    {
		        return new Constituent[0];
		    }
			return JsonConvert.DeserializeObject<List<DataOkfnConstituent>>(json).Select(a => new Constituent(a.Symbol, a.Name));
		}

		private class DataOkfnConstituent
		{
			public string Symbol { get; set; }
			public string Name { get; set; }
			public string Sector { get; set; }
		}
	}
}
