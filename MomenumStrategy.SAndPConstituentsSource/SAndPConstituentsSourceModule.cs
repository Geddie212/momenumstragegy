﻿using Microsoft.Practices.Unity;
using MomenumStrategy.Utils.Contituents;

namespace MomenumStrategy.SAndPConstituentsSource
{
	public class SAndPConstituentsSourceModule
	{
		public void Register(IUnityContainer container)
		{
			container.RegisterType<IDataOkfnClient, DataOkfnClient>(new TransientLifetimeManager());
			container.RegisterType<IIndexConstituentsSource, SAndPConstituentsProvider>(new TransientLifetimeManager());
		}
	}
}
