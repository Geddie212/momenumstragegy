﻿using MomenumStrategy.Utils.Notifications;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace MomenumStrategy.Client.Converters
{
	public class LogCategoryToBrush : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var category = (LogNotificationCategory) value;
			switch (category)
			{
				case LogNotificationCategory.Error:
					return Brushes.Red;
				case LogNotificationCategory.Warning:
					return Brushes.Yellow;
				case LogNotificationCategory.Success:
					return Brushes.Green;
				case LogNotificationCategory.Debug:
					return Brushes.Gray;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
