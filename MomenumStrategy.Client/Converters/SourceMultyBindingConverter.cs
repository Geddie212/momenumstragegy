﻿using MomenumStrategy.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace MomenumStrategy.Client.Converters
{
	public class SourceMultyBindingConverter : IMultiValueConverter
	{
		public virtual object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			var collection = values[0] as IEnumerable<IFtsEntity>;
			var search = values[1] as string;
			if (string.IsNullOrEmpty(search) || collection == null)
			{
				return values[0];
			}

			return collection.Where(a => a.Fit(search)).ToList();
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
