﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MomenumStrategy.Client
{
	public class YesNoTextBlock : TextBlock
	{


		public bool? BindableProp
		{
			get { return (bool?)GetValue(BindablePropProperty); }
			set { SetValue(BindablePropProperty, value); }
		}

		// Using a DependencyProperty as the backing store for BindableProp.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty BindablePropProperty =
			DependencyProperty.Register("BindableProp", typeof(bool?), typeof(YesNoTextBlock), new PropertyMetadata(null, PropertyChangedCallback));



		public string PositiveDescr
		{
			get { return (string)GetValue(PositiveDescrProperty); }
			set { SetValue(PositiveDescrProperty, value); }
		}

		// Using a DependencyProperty as the backing store for PositiveDescr.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty PositiveDescrProperty =
			DependencyProperty.Register("PositiveDescr", typeof(string), typeof(YesNoTextBlock), new PropertyMetadata("YES"));



		public string NegativeDescr
		{
			get { return (string)GetValue(NegativeDescrProperty); }
			set { SetValue(NegativeDescrProperty, value); }
		}

		// Using a DependencyProperty as the backing store for NegativeDescr.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty NegativeDescrProperty =
			DependencyProperty.Register("NegativeDescr", typeof(string), typeof(YesNoTextBlock), new PropertyMetadata("NO"));



		public bool InvertColors
		{
			get { return (bool)GetValue(InvertColorsProperty); }
			set { SetValue(InvertColorsProperty, value); }
		}

		// Using a DependencyProperty as the backing store for InvertColors.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty InvertColorsProperty =
			DependencyProperty.Register("InvertColors", typeof(bool), typeof(YesNoTextBlock), new PropertyMetadata(false));



		private static void PropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
		{
			var name = dependencyObject.GetValue(NameProperty);
			var val = (bool?)dependencyObject.GetValue(BindablePropProperty);
			if (val.HasValue)
			{
				if (val.Value)
				{
					dependencyObject.SetValue(TextProperty, dependencyObject.GetValue(PositiveDescrProperty));
					dependencyObject.SetValue(ForegroundProperty, (bool)dependencyObject.GetValue(InvertColorsProperty) ? Brushes.Red : Brushes.Green);
				}
				else
				{
					dependencyObject.SetValue(TextProperty, dependencyObject.GetValue(NegativeDescrProperty));
					dependencyObject.SetValue(ForegroundProperty, (bool)dependencyObject.GetValue(InvertColorsProperty) ? Brushes.Green : Brushes.Red);
				}
			}
		}
	}
}
