﻿using NLog;
using System.Windows;

namespace MomenumStrategy.Client
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private static readonly ILogger Log = LogManager.GetLogger(typeof(App).FullName);

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			Bootstrapper bs = new Bootstrapper();
			bs.Run();
			Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
		}

		private void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
		{
			Log.Fatal(e.Exception);
			MessageBox.Show("Unhandeled exception!");
		}
	}
}
