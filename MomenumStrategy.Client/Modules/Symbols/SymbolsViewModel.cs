﻿using MomenumStrategy.Entities;
using MomenumStrategy.Utils.Factories;
using NLog;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MomenumStrategy.Client.Modules.Symbols
{
	public interface ISymbolsViewModel
	{
		void RunLoadSymbols();
	}

	public class SymbolsViewModel : BindableBase, ISymbolsViewModel
	{
		private readonly ILogger _logger;
		private readonly IStocksRepository _stocksRepository;
		private IEnumerable<Stock> _stocks;
		private string _searchString;
		private readonly Task _updateSymbolsTask;
		private readonly CancellationTokenSource _cancellationTokenSource;
		private readonly ILogMessager _logMessager;

		public string SearchString
		{
			get { return _searchString; }
			set { SetProperty(ref _searchString, value, "SearchString"); }
		}

		public IEnumerable<Stock> Stocks
		{
			get { return _stocks; }
			set { SetProperty(ref _stocks, value, "Stocks"); }
		}


		public SymbolsViewModel(IStocksRepository stocksRepository, ILogMessager logMessager)
		{
			_stocksRepository = stocksRepository;
			_logMessager = logMessager;
			_cancellationTokenSource = new CancellationTokenSource();
			_updateSymbolsTask = new Task(() =>
			{
				while (!_cancellationTokenSource.IsCancellationRequested)
				{
					try
					{
						Stocks = _stocksRepository.All().OrderBy(a=>a.Symbol).ToList();
						var uuuu = Stocks.Where(a => a.GapAndSmaCaculationDate != null).ToList();
					}
					catch (Exception exc)
					{
						_logger.Error(exc);
						_logMessager.SendErrorEvent($"Failed update stocks. error [{exc.Message}]");

					}
					_cancellationTokenSource.Token.WaitHandle.WaitOne(5000);
				}
			}, _cancellationTokenSource.Token);

			_logger = LogManager.GetLogger(this.GetType().FullName);
		}

		public void RunLoadSymbols()
		{
			if (_updateSymbolsTask.Status != TaskStatus.Created)
			{
				return;
			}

			_updateSymbolsTask.Start();
		}
	}
}
