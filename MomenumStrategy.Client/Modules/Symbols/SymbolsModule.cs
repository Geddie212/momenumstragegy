﻿using Prism.Regions;

namespace MomenumStrategy.Client.Modules.Symbols
{
	public class SymbolsModule : BaseModule<SymbolsView>
	{
		public SymbolsModule(IRegionManager regionManager) : base(regionManager)
		{
		}
	}
}
