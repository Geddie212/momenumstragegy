﻿using System.Windows.Controls;

namespace MomenumStrategy.Client.Modules.Symbols
{
	/// <summary>
	/// Interaction logic for SymbolsView.xaml
	/// </summary>
	public partial class SymbolsView : UserControl
	{
		private readonly ISymbolsViewModel _symbolsViewModel;

		public SymbolsView()
		{
			InitializeComponent();
		}

		public SymbolsView(ISymbolsViewModel viewModel) : this()
		{
			DataContext = _symbolsViewModel = viewModel;
			Loaded += SymbolsView_Loaded;
		}

		private void SymbolsView_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
			_symbolsViewModel.RunLoadSymbols();
		}
	}
}
