﻿using Prism.Modularity;
using Prism.Regions;

namespace MomenumStrategy.Client.Modules.Account
{
	public class AccountModule : IModule
	{
		private readonly IRegionManager _regionManager;

		public AccountModule(IRegionManager regionManager)
		{
			_regionManager = regionManager;
		}
		public void Initialize()
		{
			_regionManager.RegisterViewWithRegion("AccountRegion", typeof(AccountView));
		}
	}
}
