﻿using System.Windows.Controls;

namespace MomenumStrategy.Client.Modules.Account
{
	/// <summary>
	/// Interaction logic for AccountView.xaml
	/// </summary>
	public partial class AccountView : UserControl
	{
		public AccountView()
		{
			InitializeComponent();
		}



		public AccountView(IAccountViewModel viewModel)
			: this()
		{
			this.DataContext = viewModel;
		}
	}
}
