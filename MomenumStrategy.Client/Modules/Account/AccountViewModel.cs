﻿using MomenumStrategy.TWS;
using Prism.Mvvm;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MomenumStrategy.Client.Modules.Account
{
	public interface IAccountViewModel
	{
		Task UpdateAccountInfo();
	}

	public class AccountViewModel : BindableBase, IAccountViewModel
	{
		private readonly IAccountInfoProvider _accountInfoProvider;
		private IEnumerable<AccoutInfo> _accoutInfo;

		public IEnumerable<AccoutInfo> AccoutInfo
		{
			get { return _accoutInfo; }
			set { SetProperty(ref _accoutInfo, value, "AccoutInfo"); }
		}

		public AccountViewModel()
		{
			
		}

		public AccountViewModel(IAccountInfoProvider accountInfoProvider) : this()
		{
			_accountInfoProvider = accountInfoProvider;
		}

		public Task UpdateAccountInfo()
		{
			return _accountInfoProvider.UpdateAccountInfo().ContinueWith(r =>
			{
				AccoutInfo = r.Result;
			});
		}
	}
}
