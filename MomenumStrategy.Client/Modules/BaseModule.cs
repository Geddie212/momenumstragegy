﻿using Prism.Modularity;
using Prism.Regions;
using System.Windows.Controls;

namespace MomenumStrategy.Client.Modules
{
	public abstract class BaseModule<T>: IModule where T : ContentControl
	{
		private readonly IRegionManager _regionManager;

		protected virtual string RegionViewName { get; }

		protected BaseModule(IRegionManager regionManager)
		{
			_regionManager = regionManager;
		}
		public virtual void Initialize()
		{
			_regionManager.RegisterViewWithRegion(RegionViewName ?? typeof(T).Name, typeof(T));
		}
	}
}
