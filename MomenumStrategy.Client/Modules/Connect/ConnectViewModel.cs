﻿using MomenumStrategy.TWS;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using System;
using System.Windows.Forms;

namespace MomenumStrategy.Client.Modules.Connect
{

	public interface IConnectViewModel : IConfirmation, IInteractionRequestAware
	{

	}

	public class ConnectViewModel : BindableBase, IConnectViewModel
	{
		private readonly IConnectorService _connectorService;

		private string _ip;
		private string _port;
		private INotification _notification;
		private Action _finishInteraction;

		public string Port
		{
			get { return _port; }
			set { SetProperty(ref _port, value, "Port"); }
		}

		public string Ip
		{
			get { return _ip; }
			set { SetProperty(ref _ip, value, "Ip"); }
		}

		public DelegateCommand ConnectCommand { get; private set; }
		public DelegateCommand ExitCommand { get; private set; }

		public ConnectViewModel()
		{
			Title = "Connection";
		}
		public ConnectViewModel(IConnectorService connectorService):this()
		{
			_connectorService = connectorService;
			Ip = "127.0.0.1";
			Port = "7496";
			ConnectCommand = new DelegateCommand(() =>
			{
				try
				{
					_connectorService.Connect();
					Confirmed = true;
					FinishInteraction();
				}
				catch (Exception exc)
				{
					MessageBox.Show(exc.Message);
				}
			});
			ExitCommand=new DelegateCommand(() =>
			{
				Confirmed = false;
				FinishInteraction();
			});
		}

		public string Title { get; set; }
		public object Content { get; set; }
		public bool Confirmed { get; set; }

		public INotification Notification
		{
			get { return _notification; }
			set { SetProperty(ref _notification, value, "Notification"); }
		}

		public Action FinishInteraction
		{
			get { return _finishInteraction; }
			set { SetProperty(ref _finishInteraction, value, "FinishInteraction"); }
		}
	}
}
