﻿using Prism.Interactivity.InteractionRequest;

namespace MomenumStrategy.Client.Modules.Connect
{
	public class ConnectConfirmation : Confirmation
	{
		public string Ip { get; set; }
		public string Port { get; set; }

		public ConnectConfirmation(string ip, string port)
		{
			Ip = ip;
			Port = port;
		}
	}
}
