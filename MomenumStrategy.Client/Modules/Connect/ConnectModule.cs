﻿using Prism.Modularity;
using Prism.Regions;

namespace MomenumStrategy.Client.Modules.Connect
{
	public class ConnectModule : IModule
	{
		private readonly IRegionManager _regionManager;

		public ConnectModule(IRegionManager regionManager)
		{
			_regionManager = regionManager;
		}
		public void Initialize()
		{
			_regionManager.RegisterViewWithRegion("ConnectView", typeof(ConnectView));
		}
	}
}
