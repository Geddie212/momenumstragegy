﻿using Emesary;
using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Client.Factories;
using MomenumStrategy.Client.Models;
using MomenumStrategy.Utils;
using MomenumStrategy.Utils.Factories;
using NLog;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace MomenumStrategy.Client.Modules.Orders
{
	public interface IOrdersViewModel
	{
		void Configure();
	}

	public class OrdersViewModel : BindableBase, IOrdersViewModel, IReceiver
	{
		private readonly ILogger _logger;
		private readonly IPositionsService _positionsService;
		private IEnumerable<PositionModel> _positionInfo;
		private readonly Task _updatePoistionsTask;
		private readonly CancellationTokenSource _cancellationTokenSource;
		private readonly ILogMessager _logMessager;
		private readonly IPositionFactory _positionFactory;

		public IEnumerable<PositionModel> PositionInfo
		{
			get { return _positionInfo; }
			set { SetProperty(ref _positionInfo, value, "PositionInfo"); }
		}

		public OrdersViewModel(IPositionsService positionsService, ILogMessager logMessager, IPositionFactory positionFactory)
		{
			_logger = LogManager.GetLogger(this.GetType().FullName);
			_positionsService = positionsService;
			_logMessager = logMessager;
			_positionFactory = positionFactory;
			_cancellationTokenSource = new CancellationTokenSource();
			_updatePoistionsTask = new Task(() =>
			{
				while (!_cancellationTokenSource.IsCancellationRequested)
				{
					try
					{
						_positionsService.RequestUpdatePortfolio();
					}
					catch (Exception exc)
					{
						_logger.Error(exc);
						_logMessager.SendErrorEvent($"Failed update positions list. error [{exc.Message}]");

					}
					_cancellationTokenSource.Token.WaitHandle.WaitOne(5000);
				}
			}, _cancellationTokenSource.Token);

			GlobalTransmitter.Register(this);
			_positionsService.GetAllPositions().ContinueWith(a =>
			{
				PositionInfo = _positionFactory.Map(a.Result);
			});
		}

		public ReceiptStatus Receive(INotification message)
		{
			if (message.Value.ToString() == Consts.NotificationTypes.PositionsUpdated)
			{
				_positionsService.GetAllPositions().ContinueWith(a =>
				{
					PositionInfo = _positionFactory.Map(a.Result);
				});

			}

			return ReceiptStatus.OK;
		}

		public void Configure()
		{
			if (_updatePoistionsTask.Status != TaskStatus.Created)
			{
				return;
			}
			_updatePoistionsTask.Start();
		}
	}
}
