﻿using Prism.Regions;

namespace MomenumStrategy.Client.Modules.Orders
{
	public class OrderModule : BaseModule<OrdersView>
	{
		private readonly IRegionManager _regionManager;
		public OrderModule(IRegionManager regionManager) : base(regionManager)
		{
			_regionManager = regionManager;
		}

		public override void Initialize()
		{
			base.Initialize();
			_regionManager.RegisterViewWithRegion("HedgeOrdersView", typeof(OrdersView));
		}
	}
}
