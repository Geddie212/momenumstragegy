﻿using System.Windows.Controls;

namespace MomenumStrategy.Client.Modules.Orders
{
	/// <summary>
	/// Interaction logic for OrdersView.xaml
	/// </summary>
	public partial class OrdersView : UserControl
	{
		private readonly IOrdersViewModel _ordersViewModel;

		public OrdersView()
		{
			InitializeComponent();
		}

		public OrdersView(IOrdersViewModel viewModel) : this()
		{
			DataContext = _ordersViewModel = viewModel;
			Loaded += OrdersView_Loaded;
		}

		private void OrdersView_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
			_ordersViewModel.Configure();
		}
	}
}
