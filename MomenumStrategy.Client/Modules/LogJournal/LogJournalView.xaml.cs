﻿using System.Windows.Controls;

namespace MomenumStrategy.Client.Modules.LogJournal
{
	/// <summary>
	/// Interaction logic for LogJournalView.xaml
	/// </summary>
	public partial class LogJournalView : UserControl
	{
		public LogJournalView()
		{
			InitializeComponent();
		}

		public LogJournalView(ILogJournalViewModel viewModel) : this()
		{
			DataContext = viewModel;
		}
	}
}
