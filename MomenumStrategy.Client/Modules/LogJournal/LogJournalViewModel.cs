﻿using Emesary;
using MomenumStrategy.Utils;
using MomenumStrategy.Utils.Notifications;
using NLog;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.Client.Modules.LogJournal
{
	public interface ILogJournalViewModel
	{
		
	}

	public class LogJournalViewModel : BindableBase, ILogJournalViewModel, IReceiver
	{
		private const int MaxEventsCount = 500;
		private readonly ILogger _logger;
		private readonly object _syncObject = new object();
		private List<LogNotification> _events;
		private bool _showErrors;
		private bool _showWarnings;
		private bool _showSuccess;
		private bool _showDebug;

		public bool ShowDebug
		{
			get { return _showDebug; }
			set { SetProperty(ref _showDebug, value, "ShowDebug");
				OnPropertyChanged("Events");
			}
		}

		public bool ShowSuccess
		{
			get { return _showSuccess; }
			set { SetProperty(ref _showSuccess, value, "ShowSuccess");
				OnPropertyChanged("Events");
			}
		}


		public bool ShowErrors
		{
			get { return _showErrors; }
			set { SetProperty(ref _showErrors, value, "ShowErrors");
				OnPropertyChanged(() => Events);
			}
		}

		public bool ShowWarnings
		{
			get { return _showWarnings; }
			set { SetProperty(ref _showWarnings, value, "ShowWarnings");
				OnPropertyChanged("Events");
			}
		}


		public IEnumerable<LogNotification> Events
		{
			get
			{
				return _events.Where(a => (_showDebug || a.Category != LogNotificationCategory.Debug) &&
				                          (_showSuccess || a.Category != LogNotificationCategory.Success) &&
										  (_showWarnings || a.Category != LogNotificationCategory.Warning) &&
										  (_showErrors || a.Category != LogNotificationCategory.Error));
			}
		}

		public LogJournalViewModel()
		{
			_events = new List<LogNotification>();
			_logger = LogManager.GetLogger(this.GetType().FullName);
			GlobalTransmitter.Register(this);
			ShowErrors = true;
			ShowWarnings = true;
			ShowSuccess = true;
			ShowDebug = false;
		}

		public ReceiptStatus Receive(INotification message)
		{
			if (message.Value.ToString() == Consts.NotificationTypes.LogIt)
			{
				var logNotification = (LogNotification) message;
				_logger.Info($"{Enum.GetName(typeof(LogNotificationCategory), logNotification.Category)} {logNotification.Message}");
				var eventsCopy = _events.ToList();
				lock (_syncObject)
				{
					eventsCopy.Insert(0, logNotification);
					_events = eventsCopy;
					OnPropertyChanged("Events");
					if (_events.Count > MaxEventsCount)
					{
						_events.Remove(_events.Last());
					}
				}
			}

			return ReceiptStatus.OK;
		}
	}
}
