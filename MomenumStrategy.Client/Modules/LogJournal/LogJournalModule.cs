﻿using Prism.Regions;

namespace MomenumStrategy.Client.Modules.LogJournal
{
	public class LogJournalModule : BaseModule<LogJournalView>
	{
		public LogJournalModule(IRegionManager regionManager) : base(regionManager)
		{
		}
	}
}
