﻿using System.Windows;
using System.Windows.Controls;

namespace MomenumStrategy.Client.Modules.LogJournal
{
	public class LogEventsFilter : Control
	{
		public bool ShowErrors
		{
			get { return (bool)GetValue(ShowErrorsProperty); }
			set { SetValue(ShowErrorsProperty, value); }
		}

		// Using a DependencyProperty as the backing store for ShowErrors.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ShowErrorsProperty =
			DependencyProperty.Register("ShowErrors", typeof(bool), typeof(LogEventsFilter), new PropertyMetadata(true));

		public bool ShowWarnings
		{
			get { return (bool)GetValue(ShowWarningsProperty); }
			set { SetValue(ShowWarningsProperty, value); }
		}

		// Using a DependencyProperty as the backing store for ShowWarnings.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ShowWarningsProperty =
			DependencyProperty.Register("ShowWarnings", typeof(bool), typeof(LogEventsFilter), new PropertyMetadata(true));

		public bool ShowSuccess
		{
			get { return (bool)GetValue(ShowSuccessProperty); }
			set { SetValue(ShowSuccessProperty, value); }
		}

		// Using a DependencyProperty as the backing store for ShowSuccess.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ShowSuccessProperty =
			DependencyProperty.Register("ShowSuccess", typeof(bool), typeof(LogEventsFilter), new PropertyMetadata(true));

		/// <summary>
		/// Returns false by default
		/// </summary>

		public bool ShowDebug
		{
			get { return (bool)GetValue(ShowDebugProperty); }
			set { SetValue(ShowDebugProperty, value); }
		}

		// Using a DependencyProperty as the backing store for ShowDebug.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ShowDebugProperty =
			DependencyProperty.Register("ShowDebug", typeof(bool), typeof(LogEventsFilter), new PropertyMetadata(false));

		//protected override void OnStyleChanged(Style oldStyle, Style newStyle)
		//{
		//	base.OnStyleChanged(oldStyle, newStyle);
		//	Console.WriteLine(oldStyle.ToString());
		//	Console.WriteLine(newStyle.ToString());
		//}

		//protected override void OnTemplateChanged(ControlTemplate oldTemplate, ControlTemplate newTemplate)
		//{
		//	base.OnTemplateChanged(oldTemplate, newTemplate);
		//	Console.WriteLine(oldTemplate.ToString());
		//	Console.WriteLine(newTemplate.ToString());
		//}

		static LogEventsFilter()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(LogEventsFilter), new FrameworkPropertyMetadata(typeof(LogEventsFilter)));
		}

	}
}
