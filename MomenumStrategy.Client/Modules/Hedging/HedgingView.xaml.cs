﻿namespace MomenumStrategy.Client.Modules.Hedging
{
	/// <summary>
	/// Interaction logic for HedgingView.xaml
	/// </summary>
	public partial class HedgingView
	{
		public HedgingView()
		{
			InitializeComponent();
		}

		public HedgingView(IHedgingViewModel viewModel) : this()
		{
			DataContext = viewModel;
		}
	}
}
