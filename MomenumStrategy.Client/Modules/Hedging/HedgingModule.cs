﻿using Prism.Regions;

namespace MomenumStrategy.Client.Modules.Hedging
{
	public class HedgingModule : BaseModule<HedgingView>
	{
		public HedgingModule(IRegionManager regionManager) : base(regionManager)
		{
		}
	}
}
