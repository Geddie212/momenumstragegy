﻿using MomenumStrategy.Applications;
using Prism.Commands;
using Prism.Mvvm;
using System.Threading.Tasks;

namespace MomenumStrategy.Client.Modules.Hedging
{
	public interface IHedgingViewModel
	{
		
	}

	public sealed class HedgingViewModel : BindableBase, IHedgingViewModel
	{
		private readonly IStrategyProcessor _strategyProcessor;
		private readonly IPositionsManager _positionsManager;
		private bool _isWorking;

		public bool IsWorking
		{
			get { return _isWorking; }
			private set { SetProperty(ref _isWorking, value, "IsWorking"); }
		}

		public DelegateCommand CloseAllPositionsCommand { get; private set; }
		public DelegateCommand RunBuyLogicCommand { get; private set; }

		public HedgingViewModel(IStrategyProcessor strategyProcessor, IPositionsManager positionsManager)
		{
			_strategyProcessor = strategyProcessor;
			_positionsManager = positionsManager;
			CloseAllPositionsCommand = new DelegateCommand(CloseAllPositions);
			RunBuyLogicCommand = new DelegateCommand(RunBuyLogicNow);
		}

		private void RunBuyLogicNow()
		{
			IsWorking = true;
			//TODO: send message to expand log
			Task.Factory.StartNew(() =>
			{
				_strategyProcessor.RunBuyingStrategy();
				IsWorking = false;
			});
		}

		private void CloseAllPositions()
		{
			IsWorking = true;
			//TODO: send message to expand log
			_positionsManager.CloseAllPositions().ContinueWith(t =>
			{
				IsWorking = false;
			});
		}
	}
}
