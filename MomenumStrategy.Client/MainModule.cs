﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using MomenumStrategy.Applications;
using MomenumStrategy.Client.Factories;
using MomenumStrategy.Client.Modules.Account;
using MomenumStrategy.Client.Modules.Connect;
using MomenumStrategy.Client.Modules.Hedging;
using MomenumStrategy.Client.Modules.LogJournal;
using MomenumStrategy.Client.Modules.Orders;
using MomenumStrategy.Client.Modules.Symbols;
using MomenumStrategy.Entities;
using MomenumStrategy.EOD.DataSource;
using MomenumStrategy.SAndPConstituentsSource;
using MomenumStrategy.TWS;
using MomenumStrategy.Utils;
using MomenumStrategy.Utils.Factories;
using MomenumStrategy.YF;

namespace MomenumStrategy.Client
{
	public sealed class MainModule
	{
		public void Register(IUnityContainer unityContainer)
		{
			unityContainer.AddNewExtension<EodDataSourceExtension>();
			unityContainer.RegisterType<ISettingsProvider, SettingsProvider>(new ContainerControlledLifetimeManager());

			unityContainer.RegisterType<IPositionsManager, PositionsManager>(new ContainerControlledLifetimeManager());
			unityContainer.RegisterType<IEmailNotificationManager, EmailNotificationManager>(new ContainerControlledLifetimeManager());

			unityContainer.RegisterType<IPositionsManager, PositionsManager>(new ContainerControlledLifetimeManager());
			unityContainer.RegisterType<ILogMessager, LogMessager>(new ContainerControlledLifetimeManager());
			unityContainer.RegisterType<IEmailService, EmailService>(new ContainerControlledLifetimeManager());
			unityContainer.RegisterType<IPositionFactory, PositionFactory>(new ContainerControlledLifetimeManager());

			unityContainer.RegisterType<IMainWindow, MainWindow>(new ContainerControlledLifetimeManager());
			unityContainer.RegisterType<IMainViewModel, MainViewModel>(new ContainerControlledLifetimeManager());

			unityContainer.RegisterType<AccountView>();
			unityContainer.RegisterType<IAccountViewModel, AccountViewModel>(new ContainerControlledLifetimeManager());

			unityContainer.RegisterType<ConnectView>();
			unityContainer.RegisterType<IConnectViewModel, ConnectViewModel>(new TransientLifetimeManager());

			unityContainer.RegisterType<SymbolsView>();
			unityContainer.RegisterType<ISymbolsViewModel, SymbolsViewModel>(new ContainerControlledLifetimeManager());

			unityContainer.RegisterType<OrdersView>();
			unityContainer.RegisterType<IOrdersViewModel, OrdersViewModel>(new ContainerControlledLifetimeManager());

			unityContainer.RegisterType<LogJournalView>();
			unityContainer.RegisterType<ILogJournalViewModel, LogJournalViewModel>(new ContainerControlledLifetimeManager());

			unityContainer.RegisterType<HedgingView>();
			unityContainer.RegisterType<IHedgingViewModel, HedgingViewModel>(new ContainerControlledLifetimeManager());

			new ApplicationsModule().Register(unityContainer);
			new YahooFinanceModule().Register(unityContainer);
			new EntitiesModule().Register(unityContainer);
			new TwsModule().Register(unityContainer);
			new SAndPConstituentsSourceModule().Register(unityContainer);
			ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(unityContainer));
		}
	}
}
