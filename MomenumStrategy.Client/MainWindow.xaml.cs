﻿using MomenumStrategy.Applications;

namespace MomenumStrategy.Client
{

	public interface IMainWindow
	{
		
	}

	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : IMainWindow
	{
		private readonly IMainViewModel _mainViewModel;

		public MainWindow()
		{
			InitializeComponent();
		}

		public MainWindow(IMainViewModel viewModel)
			: this()
		{
			this.DataContext = viewModel;
			_mainViewModel = viewModel;
			this.Loaded += MainWindow_Loaded;
		}

		private void MainWindow_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
			_mainViewModel.RaiseCustomPopupView();
		}
	}
}
