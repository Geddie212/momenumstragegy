﻿using System;

namespace MomenumStrategy.Client.Models
{
	public class PositionModel
	{
		/// <summary>This is the symbol of the underlying asset.</summary>
		public string Symbol { get; set; }

		/// <summary>
		/// This integer indicates the position on the contract.
		/// If the position is 0, it means the position has just cleared.
		/// </summary>
		public int Position { get; set; }

		/// <summary>Unit price of the instrument.</summary>
		public Decimal MarketPrice { get; set; }

		public bool? VolatilityDeviation { get; set; }
		public decimal? AdditionalAmount { get; set; }
		public bool? IsSymbolAboveMa { get; set; }
		public bool? IsTop100 { get; set; }
		public bool? HasNotGap { get; set; }
	}
}
