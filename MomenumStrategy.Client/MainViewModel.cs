﻿using Microsoft.Practices.Unity;
using MomenumStrategy.Applications;
using MomenumStrategy.Client.Modules.Account;
using MomenumStrategy.Client.Modules.Connect;
using MomenumStrategy.Entities;
using MomenumStrategy.Utils;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using System;
using System.Windows;

namespace MomenumStrategy.Client
{
	public class MainViewModel : BindableBase, IMainViewModel
	{
		private readonly IUnityContainer _unityContainer;
		private readonly IGlobalDynamicParamRepository _globalDynamicParamRepository;
		private bool _isMainSymbolBelow;
		public InteractionRequest<IConfirmation> CustomPopupViewRequest { get; private set; }

		public bool IsMainSymbolBelow
		{
			get { return _isMainSymbolBelow; }
			set { SetProperty(ref _isMainSymbolBelow, value, "IsMainSymbolBelow"); }
		}

		public MainViewModel(IUnityContainer unityContainer, IGlobalDynamicParamRepository globalDynamicParamRepository)
		{
			_unityContainer = unityContainer;
			_globalDynamicParamRepository = globalDynamicParamRepository;
			CustomPopupViewRequest = new InteractionRequest<IConfirmation>();
		}

		public void RaiseCustomPopupView()
		{
			CustomPopupViewRequest.Raise(
				_unityContainer.Resolve<IConnectViewModel>(),
				c =>
				{
					if (c.Confirmed)
					{
						_unityContainer.Resolve<IAccountViewModel>()
							.UpdateAccountInfo()
							.ContinueWith(r =>
							{
								_unityContainer.Resolve<IStrategyProcessor>().Start();
								_unityContainer.Resolve<IConstituentsProcessor>().Start();
								UpdateIsMainSymbolBelow();
							});
					}
					else
					{
						Application.Current.Shutdown(0);
					}
				});
		}

		public void UpdateIsMainSymbolBelow()
		{
			IsMainSymbolBelow =
				!Convert.ToBoolean(_globalDynamicParamRepository.FindByKey(Consts.GlobalParamnames.IsMainSymbolAbove, "false"));
		}
	}
}
