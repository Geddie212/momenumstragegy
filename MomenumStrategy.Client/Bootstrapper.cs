﻿using Microsoft.Practices.Unity;
using MomenumStrategy.Client.Modules.Account;
using MomenumStrategy.Client.Modules.Connect;
using MomenumStrategy.Client.Modules.Hedging;
using MomenumStrategy.Client.Modules.LogJournal;
using MomenumStrategy.Client.Modules.Orders;
using MomenumStrategy.Client.Modules.Symbols;
using MomenumStrategy.Utils;
using Prism.Modularity;
using Prism.Unity;
using System.Windows;

namespace MomenumStrategy.Client
{
	public class Bootstrapper : UnityBootstrapper
	{
		protected override void ConfigureContainer()
		{
			base.ConfigureContainer();
			new MainModule().Register(Container);
		}

		protected override DependencyObject CreateShell()
		{
			Container.Resolve<IEmailNotificationManager>();
			return Container.Resolve<MainWindow>();
		}

		protected override void InitializeShell()
		{
			Application.Current.MainWindow.Show();
		}

		protected override void ConfigureModuleCatalog()
		{
			ModuleCatalog catalog = (ModuleCatalog)ModuleCatalog;
			catalog.AddModule(typeof(AccountModule));
			catalog.AddModule(typeof(ConnectModule));
			catalog.AddModule(typeof(SymbolsModule));
			catalog.AddModule(typeof(OrderModule));
			catalog.AddModule(typeof(LogJournalModule));
			catalog.AddModule(typeof(HedgingModule));
		}
	}
}
