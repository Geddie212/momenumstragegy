﻿using MomenumStrategy.Client.Models;
using MomenumStrategy.Entities;
using MomenumStrategy.TWS;
using MomenumStrategy.Utils;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.Client.Factories
{
	public interface IPositionFactory
	{
		IEnumerable<PositionModel> Map(IEnumerable<PositionInfo> positionInfos);
	}
	public class PositionFactory : IPositionFactory
	{
		private readonly IStocksRepository _stocksRepository;
		private readonly ISettingsProvider _settingsProvider;

		public PositionFactory(IStocksRepository stocksRepository, ISettingsProvider settingsProvider)
		{
			_stocksRepository = stocksRepository;
			_settingsProvider = settingsProvider;
		}

		public IEnumerable<PositionModel> Map(IEnumerable<PositionInfo> positionInfos)
		{
			if (!positionInfos.Any())
			{
				return new PositionModel[0];
			}

			var stocks =
				_stocksRepository.GetActualStocks()
					.OrderByDescending(a => a.RankValue)
					.ToList()
					.Select((a, i) => new {entity = a, index = i})
					.ToDictionary(a => a.entity.Symbol, a => a);
			var list = new List<PositionModel>();
			foreach (var positionInfo in positionInfos)
			{
				var model = new PositionModel
				{
					Symbol = positionInfo.Symbol,
					Position = positionInfo.Position,
					MarketPrice = positionInfo.MarketPrice
				};

				var symbolIsActual = stocks.ContainsKey(positionInfo.Symbol);
				if (symbolIsActual)
				{
					model.AdditionalAmount = stocks[positionInfo.Symbol].entity.AdditionalAmount;
					model.VolatilityDeviation = stocks[positionInfo.Symbol].entity.AdditionalAmount > 0;
					model.HasNotGap = (stocks[positionInfo.Symbol].entity.HasNotGap ?? true);
					model.IsSymbolAboveMa = stocks[positionInfo.Symbol].entity.IsStockAboveMa;
					model.IsTop100 = stocks[positionInfo.Symbol].index <
					                 _settingsProvider.GetAppConfigValue<int>(Consts.ApplicationSettings.TopRankStocksCount);
				}

				list.Add(model);
			}

			return list;
		}
	}
}
