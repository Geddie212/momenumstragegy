﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MomenumStrategy.TWS
{
	public interface IPositionInfoProvider
	{
		Task<IEnumerable<PositionInfo>> GetAllPositions();
		Task<PositionInfo> GetPositionIfExists(string twsSymbol);
		void RequestUpdatePortfolio();
	}

	public class PositionInfoProvider : IPositionInfoProvider
	{
		private readonly ITwsClient _twsClient;

		public PositionInfoProvider(ITwsClient twsClient)
		{
			_twsClient = twsClient;
		}

		public Task<IEnumerable<PositionInfo>> GetAllPositions()
		{
			return _twsClient.GetPositions();
		}

		public void RequestUpdatePortfolio()
		{
			_twsClient.RequestUpdatePortfolio();
		}

		public Task<PositionInfo> GetPositionIfExists(string twsSymbol)
		{
			return GetAllPositions().ContinueWith(a => a.Result.FirstOrDefault(r => r.Symbol == twsSymbol));
		}
	}
}
