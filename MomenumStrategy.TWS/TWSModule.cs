﻿using Microsoft.Practices.Unity;
using MomenumStrategy.TWS.Factories;

namespace MomenumStrategy.TWS
{
	public class TwsModule
	{
		public void Register(IUnityContainer container)
		{
			container.RegisterType<ITwsClient, Client>(new ContainerControlledLifetimeManager());
			container.RegisterType<IAccountInfoProvider, AccountInfoProvider>(new ContainerControlledLifetimeManager());
			container.RegisterType<IPositionInfoProvider, PositionInfoProvider>(new ContainerControlledLifetimeManager());
			container.RegisterType<IOrderImplementationProvider, OrderImplementationProvider>(new ContainerControlledLifetimeManager());
			container.RegisterType<IMarketDataProvider, MarketDataProvider>(new ContainerControlledLifetimeManager());
			
			container.RegisterType<IConnectorService, ConnectorService>(new ContainerControlledLifetimeManager());

			container.RegisterType<IPositionInfoFactory, PositionInfoFactory>(new ContainerControlledLifetimeManager());
			container.RegisterType<IOrderFactory, OrderFactory>(new ContainerControlledLifetimeManager());
			container.RegisterType<IOrderContractFactory, OrderContractFactory>(new ContainerControlledLifetimeManager());

		}
	}
}
