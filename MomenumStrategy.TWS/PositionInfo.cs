﻿using System;

namespace MomenumStrategy.TWS
{
	public sealed class PositionInfo
	{
		/// <summary>This is the symbol of the underlying asset.</summary>
		public string Symbol { get; set; }

		/// <summary>
		/// This integer indicates the position on the contract.
		/// If the position is 0, it means the position has just cleared.
		/// </summary>
		public int Position { get; set; }

		/// <summary>Unit price of the instrument.</summary>
		public Decimal MarketPrice { get; set; }
		/// <summary>The total market value of the instrument.</summary>
		public Decimal MarketValue { get; set; }

		/// <summary>
		/// The average cost per share is calculated by dividing your cost
		/// (execution price + commission) by the quantity of your position.
		/// </summary>
		public Decimal AverageCost { get; set; }

		/// <summary>
		/// The difference between the current market value of your open positions and the average cost, or Value - Average Cost.
		/// </summary>
		public Decimal UnrealizedPnl { get; set; }

		/// <summary>
		/// Shows your profit on closed positions, which is the difference between your entry execution cost
		/// (execution price + commissions to open the position) and exit execution cost ((execution price + commissions to close the position)
		/// </summary>
		public Decimal RealizedPnl { get; set; }
		public string AccountName { get; set; }
	}
}
