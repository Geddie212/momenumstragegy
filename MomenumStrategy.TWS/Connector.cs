﻿using Krs.Ats.IBNet;
using System.Threading.Tasks;

namespace MomenumStrategy.TWS
{
	internal class Connector
	{
		private readonly IBClient _twsClient;
		private readonly Task _reconnectTask;
		private volatile bool _taskIsRunning;
		private readonly object _syncObject;

		public bool IsConnected
		{
			get { return _twsClient.Connected; }
		}

		public Connector(IBClient twsClient)
		{
			_syncObject = new object();
			_taskIsRunning = false;
			_twsClient = twsClient;
			_reconnectTask = new Task(Reconnect);
		}

		private void Reconnect()
		{
			while (true)
			{
				if (!IsConnected)
				{
					Connect();
				}
			}
		}

		public void Connect()
		{
			_twsClient.Connect("127.0.0.1", 7496, 1);
			lock (_syncObject)
			{
				if (!_taskIsRunning)
				{
					_reconnectTask.Start();
					_taskIsRunning = true;
				}
			}
		}
    }
}
