﻿using Emesary;
using Krs.Ats.IBNet;
using MomenumStrategy.TWS.Factories;
using MomenumStrategy.Utils;
using MomenumStrategy.Utils.Factories;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MomenumStrategy.TWS
{
	public interface ITwsClient : IDisposable
	{
		Task<IEnumerable<AccoutInfo>> GetAccountInto();
		Task<IEnumerable<AccoutInfo>> UpdateAccountInfo(string account);
		Task<IEnumerable<PositionInfo>> GetPositions();
		Task<IEnumerable<OpenedOrder>> GetAllOrders();
		void RequestAllOpenOrders();
		void RequestUpdatePortfolio();

		void Connect();
		void Buy(string symbol, int amount, string currency);
		void Sell(string symbol, int amount, string currency);
		string CurrentAccount { get; }
		void RequestMarketDataFor(int symbolid, string symbol, string currency);
	}

	public class Client : ITwsClient
	{
		private readonly ILogger _logger;
		private readonly Krs.Ats.IBNet.IBClient _twsClient;
		private readonly IPositionInfoFactory _positionInfoFactory;
		private readonly IOrderFactory _orderFactory;
		private readonly IOrderContractFactory _contractFactory;
		private readonly ILogMessager _logMessager;
		private readonly Connector _connector;
		private readonly ManualResetEvent _resetEvent;
		private readonly Object _syncObject = new object();
		private string _currentAccount;
		private int _orderId;
		private readonly ConcurrentDictionary<string, PositionInfo> _positions;
		private readonly ConcurrentDictionary<int, OpenedOrder> _orders;
		private readonly ConcurrentDictionary<string, AccoutInfo> _accountInfos;
		public string CurrentAccount => _currentAccount;

		public Client(IPositionInfoFactory positionInfoFactory, IOrderFactory orderFactory,
			IOrderContractFactory contractFactory, ILogMessager logMessager)
		{
			_positionInfoFactory = positionInfoFactory;
			_orderFactory = orderFactory;
			_contractFactory = contractFactory;
			_logMessager = logMessager;
			_resetEvent = new ManualResetEvent(false);
			_positions = new ConcurrentDictionary<string, PositionInfo>();
			_orders = new ConcurrentDictionary<int, OpenedOrder>();
			_accountInfos = new ConcurrentDictionary<string, AccoutInfo>();
			_twsClient = new Krs.Ats.IBNet.IBClient();
			_connector = new Connector(_twsClient);
			_twsClient.Error += OnError;
			_twsClient.NextValidId += _twsClient_NextValidId;
			_twsClient.ConnectionClosed += _twsClient_ConnectionClosed;
			_twsClient.UpdatePortfolio += _twsClient_UpdatePortfolio;
			_twsClient.OpenOrder += TwsClientOnOpenOrder;
			_twsClient.OrderStatus += TwsClientOnOrderStatus;
			_twsClient.OpenOrderEnd += TwsClientOnOpenOrderEnd;
			_twsClient.TickPrice += _twsClient_TickPrice;
			_twsClient.HistoricalData += _twsClient_HistoricalData;
			_logger = LogManager.GetLogger(this.GetType().FullName);
		}

		private void _twsClient_HistoricalData(object sender, HistoricalDataEventArgs e)
		{
			Console.WriteLine(e.Close);
		}

		private void _twsClient_TickPrice(object sender, TickPriceEventArgs e)
		{
			Console.WriteLine(e.Price);
		}

		private void _twsClient_ConnectionClosed(object sender, ConnectionClosedEventArgs e)
		{
			_logMessager.SendErrorEvent("Connection has been closed");
		}

		private void TwsClientOnOpenOrderEnd(object sender, EventArgs eventArgs)
		{
			Task.Factory.StartNew(() => GlobalTransmitter.NotifyAll(new Notification(Consts.NotificationTypes.OrdersUpdated)));
		}

		private void TwsClientOnOrderStatus(object sender, OrderStatusEventArgs args)
		{
			_orders.AddOrUpdate(args.OrderId, new OpenedOrder
			{
				//TODO: update some info here
				OrderId = args.OrderId,
				Status = Enum.GetName(typeof(OrderStatus), args.Status)
			}, (key, order) =>
			{
				//TODO: update some info here
				order.OrderId = args.OrderId;
				order.Status = Enum.GetName(typeof(OrderStatus), args.Status);
				return order;
			});
		}

		private void TwsClientOnOpenOrder(object sender, OpenOrderEventArgs args)
		{
			_orders.AddOrUpdate(args.OrderId, new OpenedOrder
			{
				//TODO: update some info here
				Symbol = args.Contract.Symbol,
				OrderId = args.OrderId
			}, (key, order) =>
			{
				//TODO: update some info here
				order.Symbol = args.Contract.Symbol;
				return order;
			});
		}

		private void _twsClient_UpdatePortfolio(object sender, UpdatePortfolioEventArgs e)
		{
			_positions.AddOrUpdate(e.Contract.Symbol, _positionInfoFactory.CreateFrom(e), (key, value) =>
			{
				_positionInfoFactory.Update(e, value);
				return value;
			});

			GlobalTransmitter.NotifyAll(new Notification(Consts.NotificationTypes.PositionsUpdated));
		}

		private void _twsClient_NextValidId(object sender, NextValidIdEventArgs e)
		{
			_orderId = e.OrderId;
		}

		private void OnError(object sender, ErrorEventArgs e)
		{
			_logger.Info($"Code:[{e.ErrorCode}]; Message:[{e.ErrorMsg}]; TickerId:[{e.TickerId}]");
			_logMessager.SendDebugEvent($"Code:[{e.ErrorCode}]; Message:[{e.ErrorMsg}]; TickerId:[{e.TickerId}]");
		}

		public void RequestMarketDataFor(int symbolid, string symbol, string currency)
		{
			lock (_syncObject)
				//_twsClient.RequestHistoricalData(symbolid, _contractFactory.BuildStockContract(symbol, currency), DateTime.Now,
				//	TimeSpan.FromDays(5), BarSize.FiveMinutes, HistoricalDataType.Midpoint, 0);
				_twsClient.RequestMarketData(symbolid, _contractFactory.BuildStockContract(symbol, currency), new Collection<GenericTickType> { GenericTickType.MarkPrice, GenericTickType.Inventory }, false, false);

		}

		public void RequestAllOpenOrders()
		{
			lock (_syncObject)
				_twsClient.RequestAllOpenOrders();
		}

		public void RequestUpdatePortfolio()
		{
			lock (_syncObject)
				_twsClient.RequestAccountUpdates(true, _currentAccount);
		}

		public async Task<IEnumerable<PositionInfo>> GetPositions()
		{
			return _positions.Values.Where(a => a.Position > 0).ToList();
		}

		public async Task<IEnumerable<OpenedOrder>> GetAllOrders()
		{
			return _orders.Values.ToList();
		}

		public Task<IEnumerable<AccoutInfo>> UpdateAccountInfo(string account)
		{
			return Task.Factory.StartNew<IEnumerable<AccoutInfo>>(() =>
			{
				lock (_syncObject)
				{
					var @event = new EventHandler<UpdateAccountValueEventArgs>((o, args) =>
					{
						_accountInfos.AddOrUpdate(args.Key, new AccoutInfo
						{
							Property = args.Key,
							Currency = args.Currency,
							Value = args.Value,
						}, (key, value) =>
						{
							value.Currency = args.Currency;
							value.Value = args.Value;
							return value;
						});
					});
					var @endEvent = new EventHandler<AccountDownloadEndEventArgs>((o, args) =>
					{
						_resetEvent.Set();
						_twsClient.RequestAccountUpdates(false, account);
					});
					try
					{

						_twsClient.UpdateAccountValue += @event;
						_twsClient.AccountDownloadEnd += @endEvent;
						_twsClient.RequestAccountUpdates(true, account);
						_resetEvent.WaitOne(5000);
					}
					finally
					{
						_twsClient.UpdateAccountValue -= @event;
						_twsClient.AccountDownloadEnd -= @endEvent;
						_resetEvent.Reset();
					}
				}

				return _accountInfos.Values.OrderBy(a => a.Property).ToList();
			});
		}

		public async Task<IEnumerable<AccoutInfo>> GetAccountInto()
		{
			return _accountInfos.Values.ToList().OrderBy(a => a.Property).ToList();
		}

		public void Connect()
		{
			_connector.Connect();
			var a = GetAccountsList().ContinueWith(res =>
			{
				_currentAccount = res.Result;
			});

			a.Wait(TimeSpan.FromSeconds(10));
			if (_connector.IsConnected)
			{
				_logMessager.SendSuccessEvent("Connected");
			}
			else
			{
				_logMessager.SendErrorEvent("Connection failed");
			}
		}

		public void Buy(string symbol, int amount, string currency)
		{
			lock (_syncObject)
			{
				var contract = _contractFactory.BuildStockContract(symbol, currency);
				var order = _orderFactory.BuildBuyOrder(symbol, _currentAccount, amount);
				_twsClient.PlaceOrder(_orderId, contract, order);
				Interlocked.Increment(ref _orderId);
			}
		}

		public void Sell(string symbol, int amount, string currency)
		{
			lock (_syncObject)
			{
				var contract = _contractFactory.BuildStockContract(symbol, currency);
				var order = _orderFactory.BuildSellOrder(symbol, _currentAccount, amount);
				_twsClient.PlaceOrder(_orderId, contract, order);
				Interlocked.Increment(ref _orderId);
			}

		}

		public Task<string> GetAccountsList()
		{
			return Task.Factory.StartNew<string>(() =>
			{
				string account = string.Empty;
				lock (_syncObject)
				{
					var @event = new EventHandler<ManagedAccountsEventArgs>((o, args) =>
					{
						_resetEvent.Set();
						account = args.AccountsList;
					});
					try
					{

						_twsClient.ManagedAccounts += @event;
						_twsClient.RequestManagedAccts();
						_resetEvent.WaitOne();
					}
					finally
					{
						_twsClient.ManagedAccounts -= @event;
						_resetEvent.Reset();
					}
				}

				return account;
			});
		}

		public void Dispose()
		{
			_twsClient.Error -= OnError;
			_twsClient.NextValidId -= _twsClient_NextValidId;
			_twsClient.UpdatePortfolio -= _twsClient_UpdatePortfolio;
			_twsClient.OpenOrder -= TwsClientOnOpenOrder;
			_twsClient.OrderStatus -= TwsClientOnOrderStatus;
			_twsClient.OpenOrderEnd -= TwsClientOnOpenOrderEnd;
			_twsClient.Disconnect();
			_twsClient.Dispose();
		}
	}

	public class OrderStatusInfo
	{
		public string Status { get; set; }
		public int Filled { get; set; }
		public int Remaining { get; set; }
		public decimal AverageFillPrice { get; set; }
		public int PermId { get; set; }
		public int ParentId { get; set; }
		public decimal LastFillPrice { get; set; }
		public int ClientId { get; set; }
		public string WhyHeld { get; set; }
	}

	public class AccoutInfo
	{
		public string Property { get; set; }
		public string Value { get; set; }
		public string Currency { get; set; }
	}

	public class OpenedOrder
	{
		public int OrderId { get; set; }
		public string Status { get; set; }
		public string Symbol { get; set; }
	}
}
