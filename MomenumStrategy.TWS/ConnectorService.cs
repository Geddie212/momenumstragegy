﻿using System;

namespace MomenumStrategy.TWS
{
	public interface IConnectorService
	{
		bool Connect();
	}

	public sealed class ConnectorService : IConnectorService
	{
		private readonly ITwsClient _twsClient;

		public ConnectorService(ITwsClient twsClient)
		{
			_twsClient = twsClient;
		}

		public bool Connect()
		{
			try
			{
				_twsClient.Connect();
				return true;
			}
			catch (Exception)
			{
				throw;
			}
		}
	}
}
