﻿using Krs.Ats.IBNet;

namespace MomenumStrategy.TWS.Factories
{
	public interface IOrderContractFactory
	{
		Contract BuildStockContract(string symbol, string currency);
	}

	public class OrderContractFactory : IOrderContractFactory
	{
		public Contract BuildStockContract(string symbol, string currency)
		{
			return new Contract(symbol, "SMART", SecurityType.Stock, currency);
		}
	}
}
