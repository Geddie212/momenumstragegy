﻿using Krs.Ats.IBNet;

namespace MomenumStrategy.TWS.Factories
{
	public interface IOrderFactory
	{
		Order BuildBuyOrder(string symbol, string account, int contractSize);
		Order BuildSellOrder(string symbol, string account, int contractSize);
	}

	public class OrderFactory : IOrderFactory
	{
		public Order BuildBuyOrder(string symbol, string account, int contractSize)
		{
			return new Order
			{
				Account = account,
				Action = ActionSide.Buy,
				TotalQuantity = contractSize,
				Tif = TimeInForce.GoodTillCancel
			};
		}
		public Order BuildSellOrder(string symbol, string account, int contractSize)
		{
			return new Order
			{
				Account = account,
				Action = ActionSide.Sell,
				TotalQuantity = contractSize,
				Tif = TimeInForce.GoodTillCancel
			};
		}
	}
}
