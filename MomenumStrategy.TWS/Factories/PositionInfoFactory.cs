﻿using Krs.Ats.IBNet;

namespace MomenumStrategy.TWS.Factories
{
	public interface IPositionInfoFactory 
	{
		PositionInfo CreateFrom(UpdatePortfolioEventArgs e);
		void Update(UpdatePortfolioEventArgs e, PositionInfo positionInfo);
	}
	public class PositionInfoFactory : IPositionInfoFactory
	{
		public PositionInfo CreateFrom(UpdatePortfolioEventArgs e)
		{
			return new PositionInfo
			{
				Symbol = e.Contract.Symbol,
				Position = e.Position,
				AccountName = e.AccountName,
				AverageCost = e.AverageCost,
				MarketPrice = e.MarketPrice,
				MarketValue = e.MarketValue,
				RealizedPnl = e.RealizedPnl,
				UnrealizedPnl = e.UnrealizedPnl
			};
		}

		public void Update(UpdatePortfolioEventArgs e, PositionInfo positionInfo)
		{
			positionInfo.Symbol = e.Contract.Symbol;
			positionInfo.Position = e.Position;
			positionInfo.AccountName = e.AccountName;
			positionInfo.AverageCost = e.AverageCost;
			positionInfo.MarketPrice = e.MarketPrice;
			positionInfo.MarketValue = e.MarketValue;
			positionInfo.RealizedPnl = e.RealizedPnl;
			positionInfo.UnrealizedPnl = e.UnrealizedPnl;
		}
	}
}
