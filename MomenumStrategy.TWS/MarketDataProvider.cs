﻿namespace MomenumStrategy.TWS
{
	public interface IMarketDataProvider
	{
		void RequestMktData(int symbolId, string symbol, string currency);
	}

	public class MarketDataProvider : IMarketDataProvider
	{
		private readonly ITwsClient _client;

		public MarketDataProvider(ITwsClient client)
		{
			_client = client;
		}

		public void RequestMktData(int symbolId, string symbol, string currency)
		{
			_client.RequestMarketDataFor(symbolId, symbol, currency);
		}
	}
}
