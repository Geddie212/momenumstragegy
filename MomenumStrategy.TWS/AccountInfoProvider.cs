﻿using MomenumStrategy.Utils;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace MomenumStrategy.TWS
{
	public interface IAccountInfoProvider
	{
		Task<IEnumerable<AccoutInfo>> GetAccoutInfo();
		Task<IEnumerable<AccoutInfo>> UpdateAccountInfo();
		Task<double> GetAccoutValue();
	}

	public class AccountInfoProvider : IAccountInfoProvider
	{
		private readonly ITwsClient _client;
		public AccountInfoProvider(ITwsClient client)
		{
			_client = client;
		}

		public async Task<IEnumerable<AccoutInfo>>  GetAccoutInfo()
		{
			return await _client.GetAccountInto();
		}

		public async Task<double> GetAccoutValue()
		{
			var value = (await _client.GetAccountInto()).FirstOrDefault(a => a.Property == Consts.AccountValueKeyName);
			if (value == null)
			{
				return 0.0;
			}
			double accountValue = 0;
			double.TryParse(value.Value, NumberStyles.Float | NumberStyles.Number | NumberStyles.AllowThousands,
				NumberFormatInfo.InvariantInfo, out accountValue);
			return accountValue;
		}

		public async Task<IEnumerable<AccoutInfo>> UpdateAccountInfo()
		{
			return await _client.UpdateAccountInfo(_client.CurrentAccount);
		}
	}
}
