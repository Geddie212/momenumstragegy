﻿namespace MomenumStrategy.TWS
{
	public interface IOrderImplementationProvider
	{
		void Buy(string symbol, int amount, string currency);
		void Sell(string symbol, int amount, string currency);
	}

	public class OrderImplementationProvider : IOrderImplementationProvider
	{
		private readonly ITwsClient _twsClient;

		public OrderImplementationProvider(ITwsClient twsClient)
		{
			_twsClient = twsClient;
		}

		public void Buy(string symbol, int amount, string currency)
		{
			_twsClient.Buy(symbol, amount, currency);
		}

		public void Sell(string symbol, int amount, string currency)
		{
			_twsClient.Sell(symbol, amount, currency);
		}
	}
}
