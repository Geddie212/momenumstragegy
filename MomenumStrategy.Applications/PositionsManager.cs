﻿using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Utils.Factories;
using System;
using System.Threading.Tasks;

namespace MomenumStrategy.Applications
{
	public interface IPositionsManager
	{
		Task CloseAllPositions();
	}


	public class PositionsManager : IPositionsManager
	{
		private readonly IPositionsService _positionsService;
		private readonly IOrdersService _ordersService;
		private readonly ILogMessager _logMessager;

		public PositionsManager(IPositionsService positionsService, IOrdersService ordersService, ILogMessager logMessager)
		{
			_positionsService = positionsService;
			_ordersService = ordersService;
			_logMessager = logMessager;
		}

		public async Task CloseAllPositions()
		{
			var stocks = await _positionsService.StocksInPosition();
			foreach (var stock in stocks)
			{
				try
				{
					await _ordersService.ClosePosition(stock);
				}
				catch (Exception exc)
				{
					_logMessager.SendErrorEvent($"Unalbe to close {stock} position . Cause: {exc.Message}");
				}
			}
		}
	}
}
