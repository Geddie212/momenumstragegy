﻿using MomenumStrategy.Applications.Models;
using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Applications.Serivces.Indicators;
using MomenumStrategy.TWS;
using MomenumStrategy.Utils.Factories;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MomenumStrategy.Applications.Factories
{
	public interface IFinalCaculationFactory
	{
		Task<IEnumerable<FinalCaculationResult>> CreateForBuyStrategy(IEnumerable<RankingResult> rankingResults);
		Task<IEnumerable<FinalCaculationResult>> CreateForRebalancing(IEnumerable<SymbolRebalancingInfo> rebalancingResults);
	}

	public class FinalCaculationFactory : IFinalCaculationFactory
	{
		private readonly ILogger _logger;
		private readonly ISharesCalculatorService _sharesCalculatorService;
		private readonly IAccountInfoProvider _accountInfoProvider;
		private readonly ILogMessager _logMessager;
		private readonly IRebalancingCalculator _rebalancingCalculator;

		public FinalCaculationFactory(ISharesCalculatorService sharesCalculatorService,
			IAccountInfoProvider accountInfoProvider, ILogMessager logMessager, IRebalancingCalculator rebalancingCalculator)
		{
			_sharesCalculatorService = sharesCalculatorService;
			_accountInfoProvider = accountInfoProvider;
			_logMessager = logMessager;
			_rebalancingCalculator = rebalancingCalculator;
			_logger = LogManager.GetLogger(this.GetType().FullName);
		}

		public async Task<IEnumerable<FinalCaculationResult>> CreateForBuyStrategy(IEnumerable<RankingResult> rankingResults)
		{
			var list = new List<FinalCaculationResult>();
			//double accountValue = await _accountInfoProvider.GetAccoutValue();
			//var symbolsInPositions = await _positionsService.GetAllPositions();
			//var actualPrices = _yahooDataProvider2.GetActualPrices();
			//double totalSum = symbolsInPositions.Select(a => a.Position*actualPrices[a.Symbol]).Sum();

			foreach (var rankingResult in rankingResults.OrderByDescending(a => a.Rank))
			{
				try
				{
					var shares = await _sharesCalculatorService.CalculateAmountByRisk(rankingResult.BarData, rankingResult.Symbol);
					//var currentPrice = actualPrices[rankingResult.Symbol];
					var newItem = CreateBuy(shares, rankingResult.Rank, rankingResult.Symbol, 0);
					//if (shares > 0 && (totalSum + newItem.DealValue) > accountValue)
					//{
					//	_logMessager.SendDebugEvent(
					//		$"During the strategy calculations, symbol [{rankingResult.Symbol}] has been skipped because the total sum [{totalSum}+{shares}*{currentPrice}={totalSum + shares * currentPrice}] exceeded than the account value {accountValue}");
					//	continue;
					//}
					//if (Math.Abs(newItem.DealValue) < 0.0001)
					//{
					//	_logMessager.SendDebugEvent(
					//		$"During the strategy calculations, symbol [{rankingResult.Symbol}] has been skipped because canculated amount of shares is 0 or current price is 0");
					//	continue;
					//}

					list.Add(newItem);
					//totalSum += newItem.DealValue;
				}
				catch (Exception exception)
				{
					_logger.Fatal(exception);
					_logMessager.SendErrorEvent($"({_logger.Name}) Unable to buy {rankingResult.Symbol} due to the following reason:[{exception.Message}]");
				}
			}

			return list;
		}

		public async Task<IEnumerable<FinalCaculationResult>> CreateForRebalancing(IEnumerable<SymbolRebalancingInfo> rebalancingResults)
		{
			var list = new List<FinalCaculationResult>();
			double totalSum = 0.0;
			double accountValue = await _accountInfoProvider.GetAccoutValue();
			foreach (var rebalancingInfo in rebalancingResults.Where(a => a.Action == RebalancingAction.Close).OrderByDescending(a => a.Rank))
			{
				var currentPrice = rebalancingInfo.BarData.Last().Close;
				var newItem = CreateToRebalance(rebalancingInfo.Amount, 0, rebalancingInfo.Rank, rebalancingInfo.Symbol, currentPrice, rebalancingInfo.Action);
				totalSum += newItem.DealValue;
			}

			//var actualPrices = _yahooDataProvider2.GetActualPrices();
			var toRebalance = rebalancingResults.Where(a => a.Action == RebalancingAction.Rebalance)
				.OrderByDescending(a => a.Rank)
				.Select(
					rebalancingInfo =>
					{
						var shares = _rebalancingCalculator.GetAdditionAmountToBuy(rebalancingInfo.Symbol).Result;
						//var currentPrice = actualPrices[rebalancingInfo.Symbol];
						var newItem = CreateToRebalance(shares, rebalancingInfo.Amount, rebalancingInfo.Rank, rebalancingInfo.Symbol, 0, rebalancingInfo.Action);
						//totalSum += newItem.DealValue;
						return newItem;

					}).ToList();

			foreach (var newItem in toRebalance)
			{
				try
				{
					//if (newItem.AdditionalAmount > 0 && (totalSum + newItem.RelanacingValue) > accountValue)
					//{
					//	_logMessager.SendDebugEvent(
					//		$"During the strategy calculations, symbol [{newItem.Symbol}] has been skipped because the total sum [{totalSum}+{newItem.AdditionalAmount}*{newItem.CurrentPrice}={totalSum + newItem.AdditionalAmount * newItem.CurrentPrice}] exceeded than the account value {accountValue}");
					//	continue;
					//}
					//if (newItem.AdditionalAmount == 0)
					//{
					//	_logMessager.SendDebugEvent(
					//		$"During the strategy calculations, symbol [{newItem.Symbol}] has been skipped because canculated amount of shares is 0");
					//	continue;
					//}

					list.Add(newItem);
					//totalSum += newItem.DealValue;
				}
				catch (Exception exception)
				{
					_logger.Fatal(exception);
					_logMessager.SendErrorEvent($"({_logger.Name}) Unable to buy {newItem.Symbol} due to the following reason:[{exception.Message}]");
				}
			}

			return list;
		}

		public FinalCaculationResult CreateBuy(int currentAmount, double rank, string symbol, double currentPrice)
		{
			return new FinalCaculationResult
			{
				CurrentAmount = currentAmount,
				CurrentPrice = currentPrice,
				Rank = rank,
				Symbol = symbol,
				BuyOrSell = OrderType.Buy
			};
		}

		public FinalCaculationResult CreateToRebalance(int additionalAmount, int amount, double rank, string symbol, double currentPrice, RebalancingAction action)
		{
			return new FinalCaculationResult
			{
				AdditionalAmount = additionalAmount,
				CurrentAmount = amount,
				CurrentPrice = currentPrice,
				Rank = rank,
				Symbol = symbol,
				BuyOrSell = action == RebalancingAction.Close ? OrderType.Sell : OrderType.Buy
			};
		}
	}
}
