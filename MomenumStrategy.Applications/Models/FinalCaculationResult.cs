﻿namespace MomenumStrategy.Applications.Models
{
	public class FinalCaculationResult
	{
		public string Symbol { get; set; }
		public double Rank { get; set; }
		public double CurrentPrice { get; set; }
		public int CurrentAmount { get; set; }
		public OrderType BuyOrSell { get; set; }
		public double DealValue => CurrentPrice * CurrentAmount* (int) BuyOrSell;
		public double RelanacingValue => CurrentPrice * AdditionalAmount;
		public int AdditionalAmount { get; set; }
	}
}
