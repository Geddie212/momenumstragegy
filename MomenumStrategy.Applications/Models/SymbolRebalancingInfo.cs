﻿using MomenumStrategy.Applications.Serivces;
using System.Collections.Generic;

namespace MomenumStrategy.Applications.Models
{
	public class SymbolRebalancingInfo
	{
		public int Amount { get; set; }
		public RebalancingAction Action { get; set; }
		public string Symbol { get; set; }
		public double Rank { get; set; }
		public IEnumerable<Bar> BarData { get; set; }
	}

	public enum RebalancingAction
	{
		Open =1,
		Close = 2,
		Rebalance = 3,
	}
}
