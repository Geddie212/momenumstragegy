﻿namespace MomenumStrategy.Applications.Models
{
	public enum OrderType
	{
		Buy = 1,
		Sell = -1,
	}
}
