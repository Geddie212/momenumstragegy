﻿using Microsoft.Practices.ObjectBuilder2;
using MomenumStrategy.Applications.Factories;
using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Entities;
using MomenumStrategy.Utils;
using MomenumStrategy.Utils.Exceptions;
using MomenumStrategy.Utils.Factories;
using NLog;
using System;
using System.Linq;

namespace MomenumStrategy.Applications.Strategy
{
	public interface IBuyStrategy : IStrategy
	{

	}

	public class BuyStrategy : IBuyStrategy
	{
		private readonly ILogger _logger;
		private readonly IOrdersService _ordersService;
		private readonly IStrategyRulesChecker _strategyRulesChecker;
		private readonly IHistoricalDataService _historicalDataService;
		private readonly IStocksRepository _stocksRepository;
		private readonly IRankingService _rankingService;
		private readonly ISettingsProvider _settingsProvider;
		private readonly ILogMessager _logMessager;
		private readonly IFinalCaculationFactory _finalCaculationFactory;
		private readonly IPositionsService _positionsService;
		private readonly IGlobalDynamicParamRepository _globalDynamicParamRepository;

		public BuyStrategy(IOrdersService ordersService, IStrategyRulesChecker strategyRulesChecker,
			IHistoricalDataService historicalDataService, IStocksRepository stocksRepository, ISettingsProvider settingsProvider,
			IRankingService rankingService, ILogMessager logMessager, IFinalCaculationFactory finalCaculationFactory, IPositionsService positionsService, IGlobalDynamicParamRepository globalDynamicParamRepository)
		{
			_ordersService = ordersService;
			_strategyRulesChecker = strategyRulesChecker;
			_historicalDataService = historicalDataService;
			_stocksRepository = stocksRepository;
			_settingsProvider = settingsProvider;
			_rankingService = rankingService;
			_logMessager = logMessager;
			_finalCaculationFactory = finalCaculationFactory;
			_positionsService = positionsService;
			_globalDynamicParamRepository = globalDynamicParamRepository;
			_logger = LogManager.GetLogger(this.GetType().FullName);
		}

		public async void RunOnce()
		{
			try
			{
				if (!_strategyRulesChecker.IsItWednesday() && !_settingsProvider.GetAppConfigValue<bool>(Consts.ApplicationSettings.DisableTheRuleIfItsWednesday))
				{
					return;
				}

				var mainSymbol =
					_settingsProvider.GetAppConfigValue<string>(Consts.ApplicationSettings.MainIndex);
				var sAndPData =
					_historicalDataService.GetBarData(mainSymbol);

				{
					bool exceptionRiased;
					if (
						!CatchNotEnoughDataExceptionIfItIs(
							() =>
							{
								var value = _strategyRulesChecker.IsSAndP500IndexAbove200Ma(sAndPData.Select(a => a.Close).ToArray());
								CommitScope.ExecInScope(() =>
								{
									_globalDynamicParamRepository.AddOrUpdate(Consts.GlobalParamnames.IsMainSymbolAbove, value.ToString());
								});
								return value;
							}, mainSymbol,
							out exceptionRiased) || exceptionRiased)
					{
						return;
					}
				}

				var stocks = _stocksRepository
					.GetActualStocks();
				var ranckingData = _rankingService.Rank(stocks.Select(a => a.Symbol))
					.Where(a => a != null)
					.OrderByDescending(a => a.Rank).ToList()
					.Take(_settingsProvider.GetAppConfigValue<int>(Consts.ApplicationSettings.TopRankStocksCount)).ToList();
				_logMessager.SendSuccessEvent($"({_logger.Name}) Ranking has been finished");
				var symbolsToBuy =
					ranckingData.Where(a =>
					{
						bool exceptionRiased;
						return CatchNotEnoughDataExceptionIfItIs(
							() =>
							{
								var isStockAboveMa = _strategyRulesChecker.IsStockAboveMa(a.BarData.Select(b => b.Close).ToArray(), 100);
								var hasNotHadGap = _strategyRulesChecker.HasNotHadGap(a.BarData);
								CommitScope.ExecInScope(() =>
								{
									var stock = _stocksRepository.FindBySymbol(a.Symbol);
									stock.HasNotGap = hasNotHadGap;
									stock.IsStockAboveMa = isStockAboveMa;
									stock.GapAndSmaCaculationDate = DateTime.Now;
								});
								return isStockAboveMa && hasNotHadGap;
							}, a.Symbol, out exceptionRiased) && !exceptionRiased;
					});


				var symbolsInPositions = await _positionsService.GetAllPositions();
				var finalResult =
					_finalCaculationFactory.CreateForBuyStrategy(
						symbolsToBuy.ToList()).Result;
				finalResult.Where(a => symbolsInPositions.All(sip => sip.Symbol != a.Symbol)).ForEach(a =>
				{
					try
					{
						_logMessager.SendDebugEvent($"({_logger.Name}) Buying [{a.Symbol}] amount is {a.CurrentAmount}]");
						_ordersService.Buy(a.Symbol, a.CurrentAmount);
					}
					catch (Exception exception)
					{
						_logger.Fatal(exception);
						_logMessager.SendErrorEvent($"({_logger.Name}) Unable to buy {a.Symbol} due to the following reason:[{exception.Message}]");
					}
				});

			}
			catch (Exception exc)
			{
				_logger.Fatal(exc);
				_logMessager.SendErrorEvent($"{_logger.Name} has been interapted the error is:[{exc.Message}]");
				throw;
			}

		}

		public T CatchNotEnoughDataExceptionIfItIs<T>(Func<T> func, string symbol, out bool exceptionRiased)
		{
			exceptionRiased = false;
			try
			{
				return func();
			}
			catch (NotEnoughDataException dataException)
			{
				_logger.Warn(dataException);
				_logMessager.SendWarningEvent($"Symbol:{symbol}. {dataException.Message}");
				exceptionRiased = true;
			}

			return default(T);
		}
	}
}
