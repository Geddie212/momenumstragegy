﻿namespace MomenumStrategy.Applications.Strategy
{
	public interface IStrategy
	{
		void RunOnce();
	}
}
