﻿using MomenumStrategy.Applications.Factories;
using MomenumStrategy.Applications.Models;
using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Entities;
using MomenumStrategy.Utils;
using MomenumStrategy.Utils.Exceptions;
using MomenumStrategy.Utils.Factories;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.Applications.Strategy
{
	public interface IRebalancingStrategy : IStrategy
	{

	}

	public class RebalancingStrategy : IRebalancingStrategy
	{
		private readonly ILogger _logger;
		private readonly IStrategyRulesChecker _strategyRulesChecker;
		private readonly IHistoricalDataService _historicalDataService;
		private readonly IStocksRepository _stocksRepository;
		private readonly IRankingService _rankingService;
		private readonly ISettingsProvider _settingsProvider;
		private readonly IPositionsService _positionsService;
		private readonly IOrdersService _ordersService;
		private readonly ILogMessager _logMessager;
		private readonly IFinalCaculationFactory _finalCaculationFactory;

		public RebalancingStrategy(IStrategyRulesChecker strategyRulesChecker, IStocksRepository stocksRepository,
			IRankingService rankingService, ISettingsProvider settingsProvider, IPositionsService positionsService,
			IOrdersService ordersService, IHistoricalDataService historicalDataService,
			ILogMessager logMessager, IFinalCaculationFactory finalCaculationFactory)
		{
			_strategyRulesChecker = strategyRulesChecker;
			_stocksRepository = stocksRepository;
			_rankingService = rankingService;
			_settingsProvider = settingsProvider;
			_positionsService = positionsService;
			_ordersService = ordersService;
			_historicalDataService = historicalDataService;
			_logMessager = logMessager;
			_finalCaculationFactory = finalCaculationFactory;
			_logger = LogManager.GetLogger(this.GetType().FullName);
		}

		public async void RunOnce()
		{
			try
			{
				if (!_strategyRulesChecker.IsItWednesday() && !_settingsProvider.GetAppConfigValue<bool>(Consts.ApplicationSettings.DisableTheRuleIfItsWednesday))
				{
					return;
				}

				var todoList = new List<SymbolRebalancingInfo>();

				var actualStocks = _stocksRepository.GetActualStocks();
				var symbolsInPositions = await _positionsService.GetAllPositions();
				var ranckingData = _rankingService.Rank(actualStocks.Select(a => a.Symbol))
					.OrderByDescending(a => a.Rank)
					.Take(_settingsProvider.GetAppConfigValue<int>(Consts.ApplicationSettings.TopRankStocksCount)).ToList();
				_logMessager.SendSuccessEvent($"({_logger.Name}) Ranking has been finished");
				foreach (var symbolsInPosition in symbolsInPositions)
				{
					if (actualStocks.All(a => a.Symbol != symbolsInPosition.Symbol))
					{
						todoList.Add(new SymbolRebalancingInfo
						{
							Symbol = symbolsInPosition.Symbol,
							Amount = symbolsInPosition.Position,
							Action = RebalancingAction.Close
						});
					}
					else
					{
						bool exceptionRiased;
						var data = CatchNotEnoughDataExceptionIfItIs(() => _historicalDataService.GetBarData(symbolsInPosition.Symbol), symbolsInPosition.Symbol, out exceptionRiased);
						if (!exceptionRiased && !_strategyRulesChecker.IsStockAboveMa(data.Select(a => a.Close).ToArray(), 100)
							|| !_strategyRulesChecker.HasNotHadGap(data))
						{
							todoList.Add(new SymbolRebalancingInfo
							{
								Symbol = symbolsInPosition.Symbol,
								Amount = symbolsInPosition.Position,
								Action = RebalancingAction.Close
							});
						}
						else
						{
							var rankData = ranckingData.FirstOrDefault(a => a.Symbol == symbolsInPosition.Symbol);
							if (rankData != null)
							{
								todoList.Add(new SymbolRebalancingInfo
								{
									Symbol = symbolsInPosition.Symbol,
									Rank = rankData.Rank,
									BarData = data,
									Action = RebalancingAction.Rebalance
								});
							}
							else
							{
								todoList.Add(new SymbolRebalancingInfo
								{
									Symbol = symbolsInPosition.Symbol,
									Amount = symbolsInPosition.Position,
									Action = RebalancingAction.Close
								});
							}
						}
					}
				}

				var toRebalance = _finalCaculationFactory.CreateForRebalancing(todoList).Result;
				_logMessager.SendSuccessEvent($"({_logger.Name}) Rebalancing list created");
				foreach (var finalCaculationResult in toRebalance)
				{

					CommitScope.ExecInScope(() =>
					{
						var stock = _stocksRepository.FindBySymbol(finalCaculationResult.Symbol);
						stock.AdditionalAmount = finalCaculationResult.AdditionalAmount;
					});
				}

				foreach (var symbolToClose in toRebalance.Where(a => a.BuyOrSell == OrderType.Sell))
				{
					try
					{
						await _ordersService.ClosePosition(symbolToClose.Symbol);
					}
					catch (Exception exception)
					{
						_logger.Error(exception, "Closing position");
						_logMessager.SendErrorEvent($"({_logger.Name}) Unable to close position by {symbolToClose} due to the following reason:[{exception.Message}]");
					}
				}

				foreach (var finalCaculationResult in toRebalance.Where(a => a.BuyOrSell == OrderType.Buy))
				{
					try
					{
						if (finalCaculationResult.AdditionalAmount > 0)
						{
							_ordersService.Buy(finalCaculationResult.Symbol, finalCaculationResult.AdditionalAmount);
						}
						else
							_logMessager.SendDebugEvent(
								$"({_logger.Name}) {finalCaculationResult} has been going to be rebalanced but additional amount of shares is zero");
					}
					catch (Exception exception)
					{
						_logger.Error(exception, "Rebalancing (buy) position");
						_logMessager.SendErrorEvent($"({_logger.Name}) Unable to buy {finalCaculationResult} due to the following reason:[{exception.Message}]");
					}
				}
			}
			catch (Exception exc)
			{
				_logger.Error(exc);
				_logMessager.SendErrorEvent($"{_logger.Name} has been interapted the error is:[{exc.Message}]");
			}
		}
		public T CatchNotEnoughDataExceptionIfItIs<T>(Func<T> func, string symbol, out bool exceptionRiased)
		{
			exceptionRiased = false;
			try
			{
				return func();
			}
			catch (NotEnoughDataException dataException)
			{
				_logger.Warn(dataException);
				_logMessager.SendWarningEvent($"Symbol:{symbol}. {dataException.Message}");
				exceptionRiased = true;
			}

			return default(T);
		}
	}
}
