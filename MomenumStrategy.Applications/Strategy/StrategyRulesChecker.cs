﻿using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Applications.Serivces.Indicators;
using MomenumStrategy.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.Applications.Strategy
{
	public interface IStrategyRulesChecker
	{
		bool IsItWednesday();
		bool IsSAndP500IndexAbove200Ma(double[] data);
		bool IsStockAboveMa(double[] data, int maLenght);
		bool HasNotHadGap(IEnumerable<Bar> data);
	}

	public class StrategyRulesChecker : IStrategyRulesChecker
	{
		private readonly IMovingAverageService _movingAverageService;
		private readonly IGapCalculationService _gapCalculationService;
		private readonly ISettingsProvider _settingsProvider;

		public StrategyRulesChecker(IMovingAverageService movingAverageService, IGapCalculationService gapCalculationService,
			ISettingsProvider settingsProvider)
		{
			_movingAverageService = movingAverageService;
			_gapCalculationService = gapCalculationService;
			_settingsProvider = settingsProvider;
		}

		public bool IsItWednesday()
		{
			return DateTimeProvider.NowTruncated.DayOfWeek == DayOfWeek.Wednesday;
		}

		public bool IsSAndP500IndexAbove200Ma(double[] data)
		{
			var maLastValue = _movingAverageService.MovingAverageCalculator(data, 200).Last();
			var sAndPValue = data.Last();

			return sAndPValue > maLastValue;
		}

		public bool IsStockAboveMa(double[] data, int maLenght)
		{
			var maLastValue = _movingAverageService.MovingAverageCalculator(data, maLenght).Last();
			var stockValue = data.Last();

			return stockValue > maLastValue;
		}

		public bool HasNotHadGap(IEnumerable<Bar> data)
		{
			return _gapCalculationService.GetGapPercentage(data) <
			       _settingsProvider.GetAppConfigValue<double>(Consts.ApplicationSettings.StockGepThresholdInPercent);
		}
	}
}
