﻿using MomenumStrategy.TWS;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MomenumStrategy.Applications.Serivces
{
	public interface IPositionsService
	{
		Task<IEnumerable<string>> StocksInPosition();
		Task<int> GetAmountOfShares(string symbol);
		Task<IEnumerable<PositionInfo>> GetAllPositions();
		void RequestUpdatePortfolio();
	}
	public sealed class PositionsService : IPositionsService
	{
		private readonly IPositionInfoProvider _positionInfoProvider;

		public PositionsService(IPositionInfoProvider positionInfoProvider)
		{
			_positionInfoProvider = positionInfoProvider;
		}

		public async Task<IEnumerable<string>> StocksInPosition()
		{
			return await _positionInfoProvider.GetAllPositions().ContinueWith(a => a.Result.Select(r => r.Symbol));
		}

		public async Task<int> GetAmountOfShares(string symbol)
		{
			return await _positionInfoProvider.GetPositionIfExists(symbol).ContinueWith(a =>
			{
				if (a.Result != null)
				{
					return a.Result.Position;
				}

				return 0;
			});
		}

		public void RequestUpdatePortfolio()
		{
			_positionInfoProvider.RequestUpdatePortfolio();
		}

		public Task<IEnumerable<PositionInfo>> GetAllPositions()
		{
			return _positionInfoProvider.GetAllPositions();
		}
	}
}
