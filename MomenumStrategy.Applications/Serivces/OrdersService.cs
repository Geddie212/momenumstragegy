﻿using MomenumStrategy.TWS;
using MomenumStrategy.Utils;
using System.Threading.Tasks;

namespace MomenumStrategy.Applications.Serivces
{
	public interface IOrdersService
	{
		void Buy(string symbol, int count);
		Task ClosePosition(string symbol);
	}

	public class OrdersService : IOrdersService
	{
		private readonly IOrderImplementationProvider _orderImplementationProvider;
		private readonly IPositionsService _positionsService;
		private readonly ISettingsProvider _settingsProvider;

		public OrdersService(IOrderImplementationProvider orderImplementationProvider, IPositionsService positionsService, ISettingsProvider settingsProvider)
		{
			_orderImplementationProvider = orderImplementationProvider;
			_positionsService = positionsService;
			_settingsProvider = settingsProvider;
		}

		public void Buy(string symbol, int count)
		{
			// TODO: here could be a conversion from symbol to TWS symbol if they are different
			//_orderImplementationProvider.Buy(symbol, count, _settingsProvider.GetAppConfigValue<string>(Consts.ApplicationSettings.StockCurrency));
		}

		public async Task ClosePosition(string symbol)
		{
			// TODO: here could be a conversion from symbol to TWS symbol if they are different
			//var currentPosition = await _positionsService.GetAmountOfShares(symbol);
			//_orderImplementationProvider.Sell(symbol, currentPosition, _settingsProvider.GetAppConfigValue<string>(Consts.ApplicationSettings.StockCurrency));
		}
	}
}
