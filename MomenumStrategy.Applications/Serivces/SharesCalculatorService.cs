﻿using MomenumStrategy.Applications.Serivces.Indicators;
using MomenumStrategy.TWS;
using MomenumStrategy.Utils;
using MomenumStrategy.Utils.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MomenumStrategy.Applications.Serivces
{
	public interface ISharesCalculatorService
	{
		Task<int> CalculateAmountByRisk(IEnumerable<Bar> data, string symbol);
	}

	public class SharesCalculatorService : ISharesCalculatorService
	{
		private readonly IAccountInfoProvider _accountInfoProvider;
		private readonly ISettingsProvider _settingsProvider;
		private readonly IAtrService _atrService;
		private readonly ILogMessager _logMessager;

		public SharesCalculatorService(IAccountInfoProvider accountInfoProvider, ISettingsProvider settingsProvider, IAtrService atrService, ILogMessager logMessager)
		{
			_accountInfoProvider = accountInfoProvider;
			_settingsProvider = settingsProvider;
			_atrService = atrService;
			_logMessager = logMessager;
		}

		public async Task<int> CalculateAmountByRisk(IEnumerable<Bar> data, string symbol)
		{
			double accountValue = await _accountInfoProvider.GetAccoutValue();
			if (Math.Abs(accountValue) < 0.01)
				return 0;
			double riskAccount =
				_settingsProvider.GetAppConfigValue<double>(Consts.ApplicationSettings.RiskValue)*accountValue;
			var atr = 
				_atrService.AtrCalculator(data.Select(a => a.High).ToArray(), data.Select(a => a.Low).ToArray(),
					data.Select(a => a.Close).ToArray(), 20).Last();
			if (atr <= 0.0)
			{
				_logMessager.SendWarningEvent(
					$"Calculation amount of shares for [{symbol}]. ATR is zero!");
				return 0;
			}
			var result = riskAccount / atr;
			_logMessager.SendDebugEvent(
				$"Calculation amount of shares for [{symbol}]. risk*account[{riskAccount}]/art[{atr}]=[{result}]");
			return Convert.ToInt32(Math.Floor(result));


		}
	}
}
