﻿using MomenumStrategy.EOD.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.Applications.Serivces
{
	public interface IHistoricalDataService
	{
		IEnumerable<Bar> GetBarData(string symbol);
	}

	public class HistoricalDataService : IHistoricalDataService
	{
		private readonly IEodDataProvider _eodDataProvider;

		public HistoricalDataService(IEodDataProvider eodDataProvider)
		{
			_eodDataProvider = eodDataProvider;
		}

		public IEnumerable<Bar> GetBarData(string symbol)
		{
			var data = _eodDataProvider.GetEod(symbol, DateTime.Now.AddYears(-1));
			return data.OrderBy(a => a.Date).Select(a => new Bar
			{
				Close = Convert.ToDouble(a.Close),
				Open = Convert.ToDouble(a.Open),
				High = Convert.ToDouble(a.High),
				Low = Convert.ToDouble(a.Low)
			});
		}
	}

	public class Bar
	{
		public double Open { get; set; }
		public double Close { get; set; }
		public double High { get; set; }
		public double Low { get; set; }
	}
}
