﻿using MomenumStrategy.Utils;
using System;
using System.Threading.Tasks;

namespace MomenumStrategy.Applications.Serivces
{
	public interface IRebalancingCalculator
	{
		Task<int> GetAdditionAmountToBuy(string symbol);
	}

	public class RebalancingCalculator : IRebalancingCalculator
	{
		private readonly IHistoricalDataService _historicalDataService;
		private readonly IPositionsService _positionsService;
		private readonly ISharesCalculatorService _sharesCalculatorService;
		private readonly ISettingsProvider _settingsProvider;

		public RebalancingCalculator(IHistoricalDataService historicalDataService, IPositionsService positionsService,
			ISharesCalculatorService sharesCalculatorService, ISettingsProvider settingsProvider)
		{
			_historicalDataService = historicalDataService;
			_positionsService = positionsService;
			_sharesCalculatorService = sharesCalculatorService;
			_settingsProvider = settingsProvider;
		}

		public async Task<int> GetAdditionAmountToBuy(string symbol)
		{
			var data = _historicalDataService.GetBarData(symbol);
			var shares =  await _sharesCalculatorService.CalculateAmountByRisk(data, symbol);
			var currentShares =  await _positionsService.GetAmountOfShares(symbol);
			if (Math.Abs(Convert.ToDouble(shares)/Convert.ToDouble(currentShares) - 1.0) >
			    _settingsProvider.GetAppConfigValue<double>(Consts.ApplicationSettings.RebalancingShareAmountThreshold))
			{
				return shares - currentShares;
			}

			return 0;
		}
	}
}
