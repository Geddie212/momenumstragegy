﻿using MomenumStrategy.Entities;
using MomenumStrategy.Utils.Contituents;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.Applications.Serivces
{
	public interface IStocksService
	{
		void Actualize(IEnumerable<Constituent> constituents);
	}

	public class StocksService : IStocksService
	{
		private readonly IStocksRepository _stocksRepository;

		public StocksService(IStocksRepository stocksRepository)
		{
			_stocksRepository = stocksRepository;
		}

		public void Actualize(IEnumerable<Constituent> constituents)
		{
			var stocks = _stocksRepository.All();
			foreach (var stock in stocks)
			{
				CommitScope.ExecInScope(() => {
					var actual = constituents.SingleOrDefault(a => a.Symbol == stock.Symbol);
					if (actual == null)
					{
						stock.IsDMark = true;
					}
					else
					{
						stock.Name = actual.Name;
					}
				});

			}

			foreach (var constituent in constituents)
			{
				CommitScope.ExecInScope(() => {
					var current = stocks.SingleOrDefault(a => a.Symbol == constituent.Symbol);
					if (current == null)
					{
						var newStock = new Stock
						{
							Symbol = constituent.Symbol,
							Name = constituent.Name,
							Size = 1,
							IsDMark = false
						};

						_stocksRepository.Add(newStock);
					}
					else
					{
						current.IsDMark = false;
					}
				});
			}
		}
	}
}
