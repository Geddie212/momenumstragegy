﻿using MomenumStrategy.Utils.Exceptions;
using System.Linq;
using TicTacTec.TA.Library;

namespace MomenumStrategy.Applications.Serivces.Indicators
{
	public interface IMovingAverageService
	{
		double[] MovingAverageCalculator(double[] priceData, int periods);
	}

	public class MovingAverageService : IMovingAverageService
	{
		public double[] MovingAverageCalculator(double[] priceData, int periods)
		{
			if (priceData.Length < periods)
			{
				throw new NotEnoughDataException("MovingAverageCalculator", priceData.Length, periods);
			}
			//Creates an SMA of Prices
			int outBegIdx;
			int outNbElement;
			var output = new double[priceData.Length];
			Core.Sma(0, priceData.Length - 1, priceData, periods, out outBegIdx, out outNbElement, output);
			var zeros = new double[periods - 1];
			var result = zeros.ToList();
			result.AddRange(output.Take(output.Length - zeros.Length));
			return result.ToArray();
		}
	}
}
