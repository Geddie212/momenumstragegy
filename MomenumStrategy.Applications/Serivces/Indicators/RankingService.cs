﻿using MomenumStrategy.Applications.Serivces.Indicators;
using MomenumStrategy.Entities;
using MomenumStrategy.Utils.Exceptions;
using MomenumStrategy.Utils.Factories;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.Applications.Serivces
{
	public interface IRankingService
	{
		IEnumerable<RankingResult> Rank(IEnumerable<string> symbols);
	}
	public sealed class RankingService  : IRankingService
	{
		private readonly ILogger _logger;
		private readonly IHistoricalDataService _historicalDataService;
		private readonly ILnService _lnService;
		private readonly ILinearRegressionSlopeSerivce _linearRegressionSlopeSerivce;
		private readonly ILogMessager _logMessager;
		private readonly IStocksRepository _stocksRepository;

		public RankingService(IHistoricalDataService historicalDataService, ILnService lnService,
			ILinearRegressionSlopeSerivce linearRegressionSlopeSerivce, ILogMessager logMessager, IStocksRepository stocksRepository)
		{
			_historicalDataService = historicalDataService;
			_lnService = lnService;
			_linearRegressionSlopeSerivce = linearRegressionSlopeSerivce;
			_logMessager = logMessager;
			_stocksRepository = stocksRepository;
			_logger = LogManager.GetLogger(this.GetType().FullName);
		}

		public IEnumerable<RankingResult> Rank(IEnumerable<string> symbols)
		{
			var actualSymbols = _stocksRepository.GetActualStocks();
			var date = DateTime.UtcNow.AddHours(-4);
			return actualSymbols.AsParallel().Select(symbol =>
			{
				var data = _historicalDataService.GetBarData(symbol.Symbol);
				if (symbol.RankValueDate.HasValue && symbol.RankValue.HasValue && symbol.RankValueDate.Value > date)
				{
					return new RankingResult
					{
						Symbol = symbol.Symbol,
						Rank = symbol.RankValue.Value,
						BarData = data
					};
				}
				bool exceptionRiased;
				var result = CatchNotEnoughDataExceptionIfItIs(() =>
				{
					var lnData = _lnService.GetSlope(data.Select(a => a.Close).ToArray());
					var regression = _linearRegressionSlopeSerivce.GetSlope(lnData, 90);
					var rank = (Math.Pow(1 + regression.Last(), 250)) - 1;
					CommitScope.ExecInScope(() =>
					{
						var stock = _stocksRepository.FindBySymbol(symbol.Symbol);
						stock.RankValue = rank;
						stock.RankValueDate = DateTime.UtcNow;
					});

					return new RankingResult
					{
						Symbol = symbol.Symbol,
						Rank = rank,
						BarData = data
					};
				}, symbol.Symbol, (s) => CommitScope.ExecInScope(() =>
				{
					var stock = _stocksRepository.FindBySymbol(s);
					stock.RankValue = null;
					stock.RankValueDate = null;
				}), out exceptionRiased);

				return exceptionRiased
					? new RankingResult
					{
						Symbol = symbol.Symbol,
						Rank = int.MinValue,
						BarData = data
					}
					: result;
			});
		}

		public T CatchNotEnoughDataExceptionIfItIs<T>(Func<T> func, string symbol, Action<string> action, out bool exceptionRiased)
		{
			exceptionRiased = false;
			try
			{
				return func();
			}
			catch (NotEnoughDataException dataException)
			{
				_logger.Warn(dataException);
				_logMessager.SendWarningEvent($"Symbol:{symbol}. {dataException.Message}");
				exceptionRiased = true;
				action(symbol);
			}

			return default(T);
		}
	}
}
