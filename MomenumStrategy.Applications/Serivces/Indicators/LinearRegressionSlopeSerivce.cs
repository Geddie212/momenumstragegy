﻿using MomenumStrategy.Utils.Exceptions;
using System.Linq;
using TicTacTec.TA.Library;

namespace MomenumStrategy.Applications.Serivces.Indicators
{
	public interface ILinearRegressionSlopeSerivce
	{
		double[] GetSlope(double[] data, int length);
	}

	public class LinearRegressionSlopeSerivce : ILinearRegressionSlopeSerivce
	{
		public double[] GetSlope(double[] data, int length)
		{
			if (data.Length < length)
			{
				throw new NotEnoughDataException("LinearRegression", data.Length, length);
			}
			int outBegIndx;
			int outNbElement;
			double[] output = new double[data.Length];
			var retCode = Core.LinearRegSlope(0, data.Length - 1, data, length, out outBegIndx, out outNbElement, output);
			var zeros = new double[length - 1];
			var result = zeros.ToList();
			result.AddRange(output.Take(output.Length - zeros.Length));
			return result.ToArray();
		}
	}
}
