﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.Applications.Serivces.Indicators
{
	public interface IGapCalculationService
	{
		double GetGapPercentage(IEnumerable<Bar> data);
	}

	public class GapCalculationService : IGapCalculationService
	{
		public double GetGapPercentage(IEnumerable<Bar> data)
		{
			var array = data.ToArray();
			return array.Select((bar, i) => i == 0 ? 0 : GapPercentage(array[i - 1].Close, array[i].Open)).Max();
		}

		private static double GapPercentage(double lookback, double current)
		{
			if (lookback > current)
			{
				return Math.Abs((current/lookback) - 1)*100;
			}
			else
			{
				return Math.Abs((lookback/current) - 1)*100;
			}
		}
	}
}
