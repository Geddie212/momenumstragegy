﻿using System;
using System.Linq;

namespace MomenumStrategy.Applications.Serivces.Indicators
{
	public interface ILnService
	{
		double[] GetSlope(double[] data);
	}
	public class LnService : ILnService
	{
		public double[] GetSlope(double[] data)
		{
			return data.Select(a=>Math.Log(a)).ToArray();
		}
	}
}
