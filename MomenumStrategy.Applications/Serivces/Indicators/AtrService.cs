﻿using System.Linq;
using TicTacTec.TA.Library;

namespace MomenumStrategy.Applications.Serivces.Indicators
{
	public interface IAtrService
	{
		double[] AtrCalculator(double[] high, double[] low, double[] close, int periods);
	}

	public class AtrService : IAtrService
	{
		public double[] AtrCalculator(double[] high, double[] low, double[] close, int periods)
		{

			int outBegidx;
			int outNbElement;
			var output = new double[low.Length];
			Core.Atr(0, high.Length - 1, high.Select(a => (float)a).ToArray(), low.Select(a => (float)a).ToArray(), close.Select(a => (float)a).ToArray(), periods, out outBegidx, out outNbElement, output);
			var zeros = new double[periods];
			var result = zeros.ToList();
			result.AddRange(output.Take(output.Length - zeros.Length));
			return result.ToArray();
		}
	}
}
