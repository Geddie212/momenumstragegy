﻿using System.Collections.Generic;

namespace MomenumStrategy.Applications.Serivces.Indicators
{
	public class RankingResult
	{
		public double Rank { get; set; }
		public string Symbol { get; set; }
		public IEnumerable<Bar> BarData { get; set; }
	}
}
