﻿using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Utils;
using MomenumStrategy.Utils.Contituents;
using MomenumStrategy.Utils.Factories;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MomenumStrategy.Applications
{
	public interface IConstituentsProcessor
	{
		void Start();
		void Stop();
	}

	public class ConstituentsProcessor : IConstituentsProcessor
	{
		private readonly ISettingsProvider _settingsProvider;
		private readonly IIndexConstituentsSource _indexConstituentsSource;
		private readonly IStocksService _stocksService;
		private readonly ILogMessager _logMessager;
		private CancellationTokenSource _cancellationTokenSource;
		private Task _executionTask;
		private readonly object _syncObject = new object();

		public ConstituentsProcessor(ISettingsProvider settingsProvider, IIndexConstituentsSource indexConstituentsSource,
			IStocksService stocksService, ILogMessager logMessager)
		{
			_settingsProvider = settingsProvider;
			_indexConstituentsSource = indexConstituentsSource;
			_stocksService = stocksService;
			_logMessager = logMessager;
		}

		private void Action()
		{
			while (!_cancellationTokenSource.IsCancellationRequested)
			{
				_cancellationTokenSource
					   .Token
					   .WaitHandle
					   .WaitOne(TimeSpan.FromMinutes(1));

				_logMessager.SendSuccessEvent("Constituents synchonization has been started");
				lock (_syncObject)
				{
					var actualSymbols = _indexConstituentsSource.Load();
				    if (actualSymbols.Any())
				    {
					    _stocksService.Actualize(actualSymbols);
                    }
                }

				_logMessager.SendSuccessEvent("Constituents synchonization has been finished");
				_cancellationTokenSource
					.Token
					.WaitHandle
					.WaitOne(TimeSpan.FromDays(_settingsProvider.GetAppConfigValue<int>(Consts.ApplicationSettings.RunUpdateStockTimePeriodInDays)));
			}
		}

		public void Start()
		{
			_cancellationTokenSource = new CancellationTokenSource();
			_executionTask = new Task(Action, _cancellationTokenSource.Token, TaskCreationOptions.LongRunning);
			_executionTask.ContinueWith(r =>
			{
				if (r.Exception != null)
				{
					//TODO: log it
				}
			});
			_executionTask.Start();
		}

		public void Stop()
		{
			_cancellationTokenSource.Cancel();
		}
	}
}
