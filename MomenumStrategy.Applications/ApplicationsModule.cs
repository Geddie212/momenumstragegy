﻿using Microsoft.Practices.Unity;
using MomenumStrategy.Applications.Factories;
using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Applications.Serivces.Indicators;
using MomenumStrategy.Applications.Strategy;

namespace MomenumStrategy.Applications
{
	public class ApplicationsModule
	{
		public void Register(IUnityContainer container)
		{
			container.RegisterType<IAtrService, AtrService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IGapCalculationService, GapCalculationService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IHistoricalDataService, HistoricalDataService>(new ContainerControlledLifetimeManager());
			container.RegisterType<ILinearRegressionSlopeSerivce, LinearRegressionSlopeSerivce>(new ContainerControlledLifetimeManager());
			container.RegisterType<ILnService, LnService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IMovingAverageService, MovingAverageService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IOrdersService, OrdersService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IPositionsService, PositionsService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IRankingService, RankingService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IRebalancingCalculator, RebalancingCalculator>(new ContainerControlledLifetimeManager());
			container.RegisterType<ISharesCalculatorService, SharesCalculatorService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IStocksService, StocksService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IFinalCaculationFactory, FinalCaculationFactory>(new ContainerControlledLifetimeManager());
            
            container.RegisterType<IStrategyRulesChecker, StrategyRulesChecker>(new ContainerControlledLifetimeManager());
			container.RegisterType<IBuyStrategy, BuyStrategy>(new ContainerControlledLifetimeManager());
			container.RegisterType<IRebalancingStrategy, RebalancingStrategy>(new ContainerControlledLifetimeManager());

			container.RegisterType<IStrategyProcessor, StrategyProcessor>(new ContainerControlledLifetimeManager());
			container.RegisterType<IConstituentsProcessor, ConstituentsProcessor>(new ContainerControlledLifetimeManager());
		}
	}
}
