﻿using MomenumStrategy.Applications.Strategy;
using MomenumStrategy.Entities;
using MomenumStrategy.Utils;
using MomenumStrategy.Utils.Factories;
using MomenumStrategy.YF;
using NLog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MomenumStrategy.Applications
{
	public interface IStrategyProcessor
	{
		void Start();
		void Stop();
		void RunBuyingStrategy();
	}

	public class StrategyProcessor : IStrategyProcessor
	{
		private CancellationTokenSource _cancellationTokenSource;
		private readonly IRebalancingStrategy _rebalancingStrategy;
		private readonly IBuyStrategy _buyStrategy;
		private readonly ISettingsProvider _settingsProvider;
		private readonly ILogMessager _logMessager;
		private readonly ILogger _logger;
		private readonly IStocksRepository _stocksRepository;
		private readonly IYahooStreaming _yahooStreaming;
		private Task _executionTask;
		private readonly object _syncObject = new object();

		public StrategyProcessor(IRebalancingStrategy rebalancingStrategy, IBuyStrategy buyStrategy, ISettingsProvider settingsProvider, ILogMessager logMessager, IStocksRepository stocksRepository, IYahooStreaming yahooStreaming)
		{
			_rebalancingStrategy = rebalancingStrategy;
			_buyStrategy = buyStrategy;
			_settingsProvider = settingsProvider;
			_logMessager = logMessager;
			_stocksRepository = stocksRepository;
			_yahooStreaming = yahooStreaming;
			_logger = LogManager.GetLogger(typeof(StrategyProcessor).FullName);
		}

		private void Action()
		{
			while (!_cancellationTokenSource.IsCancellationRequested)
			{
				lock (_syncObject)
				{
					//var stocks = _stocksRepository.GetActualStocks();
					RunRebalancing();

					_cancellationTokenSource.Token.ThrowIfCancellationRequested();

					RunBuyingInternal();
					//var task = _yahooStreaming.RunFor(stocks.Select(a => a.Symbol)).ContinueWith(promise =>
					//{
					//	if (promise.Exception != null)
					//	{
					//		_logMessager.SendDebugEvent("Loading prices error " + promise.Exception.Message);
					//		_logger.Error(promise.Exception);
					//		return;
					//	}
					//});
					//task.Wait();
				}

				_cancellationTokenSource
					.Token
					.WaitHandle
					.WaitOne(_settingsProvider.GetAppConfigValue<int>(Consts.ApplicationSettings.RunStrategyPeriodInSeconds) * 1000);
			}
		}

		public void RunBuyingStrategy()
		{
			lock (_syncObject)
			{
				RunBuyingInternal();
			}
		}

		private void RunBuyingInternal()
		{
			try
			{
				_logMessager.SendSuccessEvent("Buy strategy is working");
				_buyStrategy.RunOnce();
				_logMessager.SendSuccessEvent("Buy strategy has been finished");
			}
			catch (Exception exception)
			{
				_logMessager.SendErrorEvent($"Buy strategy execution failed. Cause:{exception.Message}");
			}
		}

		private void RunRebalancing()
		{
			try
			{
				_logMessager.SendSuccessEvent("Rebalancsing is working");
				_rebalancingStrategy.RunOnce();
				_logMessager.SendSuccessEvent("Rebalancsing has been finished");
			}
			catch (Exception exception)
			{
				_logMessager.SendErrorEvent($"Rebalancing execution failed. Cause:{exception.Message}");
			}
		}

		public void Start()
		{
			_cancellationTokenSource = new CancellationTokenSource();
			_executionTask = new Task(Action, _cancellationTokenSource.Token, TaskCreationOptions.LongRunning);
			_executionTask.ContinueWith(r =>
			{
				if (r.Exception != null)
				{
					_logger.Error(r.Exception);
				}
			});
			_executionTask.Start();
		}

		public void Stop()
		{
			_cancellationTokenSource.Cancel();
		}
	}
}
