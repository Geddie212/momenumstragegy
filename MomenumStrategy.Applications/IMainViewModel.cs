﻿namespace MomenumStrategy.Applications
{
	public interface IMainViewModel
	{
		void RaiseCustomPopupView();

		void UpdateIsMainSymbolBelow();
	}
}
