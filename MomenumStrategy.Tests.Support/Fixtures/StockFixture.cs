﻿using MomenumStrategy.Entities;
using System;

namespace MomenumStrategy.Tests.Support.Fixtures
{
	public class StockFixture
	{
		private bool? _isDmark;

		public static StockFixture InMemory => new StockFixture();

		public static DbStockFixture InDb => new DbStockFixture();

		public virtual Stock Create()
		{
			return new Stock
			{
				Symbol = Guid.NewGuid().ToString("N").Substring(0, 4),
				Name = Guid.NewGuid().ToString("N"),
				IsDMark = _isDmark ?? false,
				Size = 1,
			};
		}

		public StockFixture WithIsDMark(bool isDmark)
		{
			_isDmark = isDmark;
			return this;
		}
	}

	public class DbStockFixture : StockFixture
	{
		public override Stock Create()
		{
			var stock = base.Create();
			StoreDbHelper<Stock>.Add(stock);
			return stock;
		}
	}
}
