﻿using MomenumStrategy.Entities;
using MomenumStrategy.Utils.Contituents;
using System;

namespace MomenumStrategy.Tests.Support.Fixtures
{
	public class ConstituetFixture
	{
		private string _symbol;
		private string _name;

		public static ConstituetFixture InMemory => new ConstituetFixture();

		public virtual Constituent Create()
		{
			var identity = Guid.NewGuid().ToString("N");
			return new Constituent(_symbol ?? identity.Substring(0, 4), _name ?? identity);
		}

		public ConstituetFixture FromStock(Stock stock)
		{
			_symbol = stock.Symbol;
			_name = stock.Name;
			return this;
		}
	}
}
