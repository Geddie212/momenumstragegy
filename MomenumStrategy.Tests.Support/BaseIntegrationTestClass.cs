﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using MomenumStrategy.Client;
using MomenumStrategy.Entities;
using NUnit.Framework;

namespace MomenumStrategy.Tests.Support
{
	public class BaseIntegrationTestClass<T>
	{
		protected T Sut;
		protected IUnityContainer UnityContainer;

		[SetUp]
		public virtual void SetUp()
		{
			UnityContainer = new UnityContainer();
			new MainModule().Register(UnityContainer);
			Sut = UnityContainer.Resolve<T>();
		}

		public void Flush()
		{
			ServiceLocator.Current.GetInstance<IMomenumStrategyUoW>().Flush();
		}


		[TearDown]
		public void TearDown()
		{
			ServiceLocator.Current.GetInstance<IMomenumStrategyUoW>().Rollback();
		}
	}
}
