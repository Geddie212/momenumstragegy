﻿using Emesary;
using Microsoft.Practices.Unity;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace MomenumStrategy.TWS.Integration.Tests
{
	[TestFixture]
	public class OrderImplementationProviderBuyTests : BaseTwsIntegrationTests<IOrderImplementationProvider>
	{
		[SetUp]
		public override void SetUp()
		{
			base.SetUp();
			UnityContainer.Resolve<ITwsClient>().Connect();
		}

		[Test]
		public void should_buy_aapl()
		{
			const string symbol = "AAPL";
			var receiver = new OrderNotificationReceiver();
			GlobalTransmitter.Register(receiver);
			Sut.Buy(symbol, 1, "USD");
			Thread.Sleep(2000);
			var client = UnityContainer.Resolve<ITwsClient>();
			client.RequestAllOpenOrders();
			receiver.WaitOne();
			var orders = client.GetAllOrders().Result;
			Assert.That(orders, Is.Not.Empty);
			Assert.That(orders.Last(a=>a.Symbol == symbol).Status, Is.Not.EqualTo("Error"));
			Assert.That(orders.Last(a => a.Symbol == symbol).Status, Is.Not.EqualTo("ApiCancelled"));
			Assert.That(orders.Last(a => a.Symbol == symbol).Status, Is.Not.EqualTo("PendingCancel"));
			Assert.That(orders.Last(a => a.Symbol == symbol).Status, Is.Not.EqualTo("ApiCancelled"));
		}


		private class OrderNotificationReceiver : IReceiver
		{
			private readonly ManualResetEvent _manualResetEvent;
			public OrderNotificationReceiver()
			{
				_manualResetEvent = new ManualResetEvent(false);
			}

			public ReceiptStatus Receive(INotification message)
			{
				_manualResetEvent.Set();
				return ReceiptStatus.OK;
			}

			public void WaitOne()
			{
				_manualResetEvent.WaitOne();
			}
		}
	}
}
