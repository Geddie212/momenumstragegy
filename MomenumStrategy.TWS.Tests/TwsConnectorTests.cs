﻿using NUnit.Framework;

namespace MomenumStrategy.TWS.Integration.Tests
{
	[TestFixture]
	public class TwsConnectorTests : BaseTwsIntegrationTests<ITwsClient>
	{
		[Test]
		public void should_connect()
		{
			Sut.Connect();
		}
	}
}
