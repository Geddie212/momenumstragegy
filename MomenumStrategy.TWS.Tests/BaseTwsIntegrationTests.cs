﻿using Microsoft.Practices.Unity;
using MomenumStrategy.Tests.Support;
using NUnit.Framework;

namespace MomenumStrategy.TWS.Integration.Tests
{
	public class BaseTwsIntegrationTests<T> : BaseIntegrationTestClass<T>
	{
		[TearDown]
		public virtual void TearDown()
		{
			var client = UnityContainer.Resolve<ITwsClient>();
			client.Dispose();
		}
	}
}
