﻿using MomenumStrategy.Applications.Factories;
using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Applications.Serivces.Indicators;
using MomenumStrategy.TWS;
using MomenumStrategy.Utils.Factories;
using MomenumStrategy.YF;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MomenumStrategy.UTests
{
	[TestFixture]
	public class FinalCaculationFactoryForBuyStrategyTests
	{
		private IFinalCaculationFactory _sut;
		private ISharesCalculatorService _sharesCalculatorService;
		private IAccountInfoProvider _accountInfoProvider;
		private ILogMessager _logMessager;
		private IRebalancingCalculator _rebalancingCalculator;
		private IYahooDataProvider2 _yahooDataProvider2;
		private IPositionsService _positionsService;
		private IEnumerable<RankingResult> _ranckingData;

		[SetUp]
		public void SetUp()
		{
			_sharesCalculatorService = Substitute.For<ISharesCalculatorService>();
			_accountInfoProvider = Substitute.For<IAccountInfoProvider>();
			_logMessager = Substitute.For<ILogMessager>();
			_rebalancingCalculator = Substitute.For<IRebalancingCalculator>();
			_yahooDataProvider2 = Substitute.For<IYahooDataProvider2>();
			_positionsService = Substitute.For<IPositionsService>();
			_sut = new FinalCaculationFactory(_sharesCalculatorService, _accountInfoProvider, _logMessager, _rebalancingCalculator);
			_accountInfoProvider.GetAccoutValue().Returns(Task.Factory.StartNew(() => 1000.0));
			_ranckingData = new List<RankingResult>
			{
				new RankingResult {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "A", Rank = 3},
				new RankingResult {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "B", Rank = 1},
				new RankingResult {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "C", Rank = 2},
			};
		}

		[Test]
		public void should_skip_symbol_with_lowest_rank_if_limit_is_exceeded()
		{
			// arrange
			_sharesCalculatorService.CalculateAmountByRisk(Arg.Any<IEnumerable<Bar>>(), Arg.Any<string>()).Returns(334);

			// act
			var result = _sut.CreateForBuyStrategy(_ranckingData);

			// assert
			_logMessager.Received().SendDebugEvent(Arg.Any<string>());
			Assert.That(result.Result.Count(), Is.EqualTo(2));
			Assert.That(result.Result.First().Symbol, Is.EqualTo("A"));
			Assert.That(result.Result.Last().Symbol, Is.EqualTo("C"));
		}

		[Test]
		public void should_add_other_symbols_even_if_exception_would_be_thrown_by_one_of_them()
		{
			// arrange
			_sharesCalculatorService
				.CalculateAmountByRisk(Arg.Any<IEnumerable<Bar>>(), "A")
				.Returns(Task.Factory.StartNew(() => 1));
			_sharesCalculatorService
				.CalculateAmountByRisk(Arg.Any<IEnumerable<Bar>>(), "B")
				.Returns(Task<int>.Factory.StartNew(() => { throw new Exception("lalala"); }));
			_sharesCalculatorService
				.CalculateAmountByRisk(Arg.Any<IEnumerable<Bar>>(), "C")
				.Returns(Task.Factory.StartNew(() => 4));

			//act
			var result = _sut.CreateForBuyStrategy(_ranckingData);

			//assert
			Assert.That(result.Result.Count(), Is.EqualTo(2));
			Assert.That(result.Result.First().Symbol, Is.EqualTo("A"));
			Assert.That(result.Result.Last().Symbol, Is.EqualTo("C"));

		}

		[Test]
		public void should_buy_if_amount_of_shares_greater_then_zero()
		{
			// arrange
			_sharesCalculatorService
				.CalculateAmountByRisk(Arg.Any<IEnumerable<Bar>>(), "A")
				.Returns(Task.Factory.StartNew(() => 1));
			_sharesCalculatorService
				.CalculateAmountByRisk(Arg.Any<IEnumerable<Bar>>(), "B")
				.Returns(Task.Factory.StartNew(() => 0));
			_sharesCalculatorService
				.CalculateAmountByRisk(Arg.Any<IEnumerable<Bar>>(), "C")
				.Returns(Task.Factory.StartNew(() => 5));

			//act
			var result = _sut.CreateForBuyStrategy(_ranckingData);

			//assert
			Assert.That(result.Result.Count(), Is.EqualTo(2));
			Assert.That(result.Result.First().Symbol, Is.EqualTo("A"));
			Assert.That(result.Result.Last().Symbol, Is.EqualTo("C"));

		}
	}
}
