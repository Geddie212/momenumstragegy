﻿using MomenumStrategy.Applications.Factories;
using MomenumStrategy.Applications.Models;
using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Applications.Serivces.Indicators;
using MomenumStrategy.Applications.Strategy;
using MomenumStrategy.Entities;
using MomenumStrategy.TWS;
using MomenumStrategy.Utils;
using MomenumStrategy.Utils.Factories;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MomenumStrategy.UTests.Strategies
{
	[TestFixture]
	public class RebalancingStrategyTests
	{
		private IRebalancingStrategy _sut;
		private string _mainSymbol;
		private IStrategyRulesChecker _strategyRulesChecker;
		private IHistoricalDataService _historicalDataService;
		private IStocksRepository _stocksRepository;
		private IRankingService _rankingService;
		private ISettingsProvider _settingsProvider;
		private IPositionsService _positionsService;
		private IOrdersService _ordersService;
		private IFinalCaculationFactory _finalCaculationFactory;
		private ILogMessager _logMessager;
		private List<string> _symbols;
		private List<Stock> _stocks;


		[SetUp]
		public void SetUp()
		{
			_strategyRulesChecker = Substitute.For<IStrategyRulesChecker>();
			_stocksRepository = Substitute.For<IStocksRepository>();
			_rankingService = Substitute.For<IRankingService>();
			_settingsProvider = Substitute.For<ISettingsProvider>();
			_positionsService = Substitute.For<IPositionsService>();
			_ordersService = Substitute.For<IOrdersService>();
			_historicalDataService = Substitute.For<IHistoricalDataService>();
			_finalCaculationFactory = Substitute.For<IFinalCaculationFactory>();
			_logMessager = Substitute.For<ILogMessager>();
			_sut = new RebalancingStrategy(_strategyRulesChecker, _stocksRepository, _rankingService, _settingsProvider,
				_positionsService, _ordersService, _historicalDataService, _logMessager, _finalCaculationFactory);
			_mainSymbol = Guid.NewGuid().ToString("N");
			_strategyRulesChecker.IsItWednesday()
				.Returns(true);
			_settingsProvider.GetAppConfigValue<int>(Consts.ApplicationSettings.TopRankStocksCount)
				.Returns(4);
			_symbols = new List<string> { "1", "2", "3", "4" };
			_stocks = new List<Stock>()
			{
				new Stock {Id =1, Name = "SomeSymbol1",Size = 1,Symbol = "1"},
				new Stock {Id =1, Name = "SomeSymbol2",Size = 1,Symbol = "2"},
				new Stock {Id =1, Name = "SomeSymbol3",Size = 1,Symbol = "3"},
				new Stock {Id =1, Name = "SomeSymbol4",Size = 1,Symbol = "4"},
			};
			_stocksRepository.GetActualStocks()
				.Returns(_stocks);
			_positionsService.GetAllPositions()
				.Returns(Task<IEnumerable<PositionInfo>>.Factory.StartNew(() => _symbols.Select(a=>new PositionInfo { Symbol = a, Position = 1})));
			_rankingService.Rank(Arg.Is<IEnumerable<string>>(a=>a.Intersect(_stocks.Select(s=>s.Symbol)).Any())).Returns(_symbols.Select((s, i) => new RankingResult
			{
				BarData = new List<Bar>(),
				Symbol = s,
				Rank = i
			}));
			_finalCaculationFactory
				.CreateForRebalancing(Arg.Is<IEnumerable<SymbolRebalancingInfo>>(a => a.Select(u=>u.Symbol).Intersect(_stocks.Select(s => s.Symbol)).Any()))
				.Returns(vls=>Task.Factory.StartNew(() => vls.Arg<IEnumerable<SymbolRebalancingInfo>>().Select((s, i) => new FinalCaculationResult()
				{
					Symbol = s.Symbol,
					Rank = i,
					CurrentAmount = 1,
					BuyOrSell = s.Action == RebalancingAction.Close ? OrderType.Sell : OrderType.Buy,
					AdditionalAmount = 1,
					CurrentPrice = 1
				})));
			_strategyRulesChecker.IsStockAboveMa(Arg.Any<double[]>(), 100).Returns(true);
			_strategyRulesChecker.HasNotHadGap(Arg.Any<IEnumerable<Bar>>()).Returns(true);
			//_finalCaculationFactory.GetAdditionAmountToBuy(Arg.Is<string>(x => _symbols.Contains(x))).Returns(1);
		}

		[Test]
		public void should_do_nothing_if_it_is_not_wednesday()
		{
			// arrange
			_strategyRulesChecker.IsItWednesday().Returns(false);

			//act
			_sut.RunOnce();

			//assert
			_positionsService.DidNotReceiveWithAnyArgs().StocksInPosition();
		}

		[Test]
		public void should_rebalance_all_positions()
		{
			// arrange


			//act
			_sut.RunOnce();

			//assert
			_positionsService.Received(1).GetAllPositions();
			_historicalDataService
				.Received(4)
				.GetBarData(Arg.Is<string>(x => _symbols.Contains(x)));
			_ordersService
				.DidNotReceive()
				.ClosePosition(Arg.Any<string>());
			_ordersService
				.Received(4)
				.Buy(Arg.Is<string>(x => _symbols.Contains(x)), 1);
		}

		[Test]
		public void should_close_one_position_becuase_of_stock_below_sma()
		{
			// arrange
			var bardata = new List<Bar>
			{
				new Bar {High = 1, Close = 2, Low = 3, Open = 4},
				new Bar {High = 5, Close = 6, Low = 7, Open = 8},
				new Bar {High = 9, Close = 10, Low = 3, Open = 11},
			};
			_historicalDataService.GetBarData("3").Returns(bardata);
			_strategyRulesChecker.IsStockAboveMa(
				Arg.Is<double[]>(x => x.Intersect(bardata.Select(a => a.Close)).Count() == 3), 100)
				.Returns(false);

			_finalCaculationFactory
				.CreateForRebalancing(Arg.Is<IEnumerable<SymbolRebalancingInfo>>(a => a.Select(u => u.Symbol).Intersect(_stocks.Select(s => s.Symbol)).Any()))
				.Returns(_symbols.Select((s, i) => new FinalCaculationResult()
				{
					Symbol = s,
					Rank = i,
					CurrentAmount = 1,
					BuyOrSell = s == "3" ? OrderType.Sell : OrderType.Buy,
					AdditionalAmount = 1,
					CurrentPrice = 1
				}));

			//act
			_sut.RunOnce();

			//assert
			_positionsService.Received(1).GetAllPositions();
			_historicalDataService
				.Received(4)
				.GetBarData(Arg.Is<string>(x => _symbols.Contains(x)));
			_ordersService
				.Received(1)
				.ClosePosition("3");
			_ordersService
				.Received(3)
				.Buy(Arg.Is<string>(x => _symbols.Contains(x)), 1);
		}

		[Test]
		public void should_close_one_position_becuase_symbol_is_not_actual_any_more()
		{
			// arrange
			_stocks = new List<Stock>()
			{
				new Stock {Id =1, Name = "SomeSymbol1",Size = 1,Symbol = "1"},
				new Stock {Id =1, Name = "SomeSymbol2",Size = 1,Symbol = "2"},
				new Stock {Id =1, Name = "SomeSymbol4",Size = 1,Symbol = "4"},
			};
			_stocksRepository.GetActualStocks()
				.Returns(_stocks);


			//act
			_sut.RunOnce();

			//assert
			_positionsService.Received(1).GetAllPositions();
			_historicalDataService
				.Received(3)
				.GetBarData(Arg.Is<string>(x => _symbols.Contains(x)));

			_finalCaculationFactory
				.Received(1)
				.CreateForRebalancing(
					Arg.Is<IEnumerable<SymbolRebalancingInfo>>(a => a.Any(s => s.Action == RebalancingAction.Close && s.Symbol == "3")));
		}

		[Test]
		public void should_close_one_position_becuase_of_gap()
		{
			// arrange
			var bardata = new List<Bar>
			{
				new Bar {High = 1, Close = 2, Low = 3, Open = 4},
				new Bar {High = 5, Close = 6, Low = 7, Open = 8},
				new Bar {High = 9, Close = 10, Low = 3, Open = 11},
			};
			_historicalDataService.GetBarData("3").Returns(bardata);
			_strategyRulesChecker.HasNotHadGap(
				Arg.Is<IEnumerable<Bar>>(x => x.Equals(bardata)))
				.Returns(false);


			//act
			_sut.RunOnce();

			//assert
			_positionsService.Received(1).GetAllPositions();
			_historicalDataService
				.Received(4)
				.GetBarData(Arg.Is<string>(x => _symbols.Contains(x)));
			_finalCaculationFactory
				.Received(1)
				.CreateForRebalancing(
					Arg.Is<IEnumerable<SymbolRebalancingInfo>>(a => a.Any(s => s.Action == RebalancingAction.Close && s.Symbol == "3")));
		}

		[Test]
		public void should_close_one_position_becuase_of_rankFileter()
		{
			// arrange

			_rankingService.Rank(Arg.Is<IEnumerable<string>>(a => a.Intersect(_stocks.Select(s => s.Symbol)).Any())).Returns(_symbols.Select((s, i) => new RankingResult
			{
				BarData = new List<Bar>(),
				Symbol = s,
				Rank = s == "3" ? 0 : i
			}));
			_settingsProvider.GetAppConfigValue<int>(Consts.ApplicationSettings.TopRankStocksCount)
				.Returns(3);


			//act
			_sut.RunOnce();

			//assert
			_positionsService.Received(1).GetAllPositions();
			_historicalDataService
				.Received(4)
				.GetBarData(Arg.Is<string>(x => _symbols.Contains(x)));
			_finalCaculationFactory
				.Received(1)
				.CreateForRebalancing(
					Arg.Is<IEnumerable<SymbolRebalancingInfo>>(a => a.Any(s => s.Action == RebalancingAction.Close && s.Symbol == "3")));
		}
	}
}
