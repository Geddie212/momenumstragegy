﻿using MomenumStrategy.Applications.Factories;
using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Applications.Serivces.Indicators;
using MomenumStrategy.Applications.Strategy;
using MomenumStrategy.Entities;
using MomenumStrategy.TWS;
using MomenumStrategy.Utils;
using MomenumStrategy.Utils.Factories;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MomenumStrategy.UTests.Strategies
{
	[TestFixture]
	public class BuyStrategyTests
	{
		private IBuyStrategy _sut;
		private IOrdersService _ordersService;
		private IStrategyRulesChecker _strategyRulesChecker;
		private IHistoricalDataService _historicalDataService;
		private IStocksRepository _stocksRepository;
		private IRankingService _rankingService;
		private ISettingsProvider _settingsProvider;
		private ILogMessager _logMessager;
		private string _mainSymbol;
		private IFinalCaculationFactory _finalCaculationFactory;
		private IPositionsService _positionsService;
		private IGlobalDynamicParamRepository _globalDynamicParamRepository;

		[SetUp]
		public void SetUp()
		{
			_ordersService = Substitute.For<IOrdersService>();
			_strategyRulesChecker = Substitute.For<IStrategyRulesChecker>();
			_historicalDataService = Substitute.For<IHistoricalDataService>();
			_stocksRepository = Substitute.For<IStocksRepository>();
			_settingsProvider = Substitute.For<ISettingsProvider>();
			_rankingService = Substitute.For<IRankingService>();
			_logMessager = Substitute.For<ILogMessager>();
			_finalCaculationFactory = Substitute.For<IFinalCaculationFactory>();
			_positionsService = Substitute.For<IPositionsService>();
			_globalDynamicParamRepository = Substitute.For<IGlobalDynamicParamRepository>();
			_sut = new BuyStrategy(_ordersService, _strategyRulesChecker, _historicalDataService, _stocksRepository,
				_settingsProvider, _rankingService, _logMessager, _finalCaculationFactory, _positionsService, _globalDynamicParamRepository);
			_mainSymbol = Guid.NewGuid().ToString("N");
			_settingsProvider.GetAppConfigValue<string>(Consts.ApplicationSettings.MainIndex)
				.Returns(_mainSymbol);
			_settingsProvider.GetAppConfigValue<int>(Consts.ApplicationSettings.TopRankStocksCount)
				.Returns(2);
			_strategyRulesChecker.IsItWednesday()
				.Returns(true);
		}

		[Test]
		public void should_do_nothing_if_it_is_not_wednesday()
		{
			// arrange
			_strategyRulesChecker.IsItWednesday().Returns(false);

			//act
			_sut.RunOnce();

			//assert
			_settingsProvider.DidNotReceiveWithAnyArgs().GetAppConfigValue<string>(Consts.ApplicationSettings.MainIndex);
			_historicalDataService.DidNotReceiveWithAnyArgs().GetBarData(Arg.Any<string>());
		}

		[Test]
		public void should_do_nothing_if_SAndP500_below_200Ma()
		{
			// arrange
			var bardata = new List<Bar>
			{
				new Bar {High = 1, Close = 2, Low = 3, Open = 4},
				new Bar {High = 5, Close = 6, Low = 7, Open = 8},
				new Bar {High = 9, Close = 10, Low = 3, Open = 11},
			};

			_historicalDataService.GetBarData(_mainSymbol).Returns(bardata);
			_strategyRulesChecker.IsSAndP500IndexAbove200Ma(
				Arg.Is<double[]>(x => x.Intersect(bardata.Select(a => a.Close)).Count() == 4)).Returns(false);

			//act
			_sut.RunOnce();

			//assert
			_strategyRulesChecker.Received(1).IsSAndP500IndexAbove200Ma(Arg.Any<double[]>());
			_settingsProvider.Received(1).GetAppConfigValue<string>(Consts.ApplicationSettings.MainIndex);
			_historicalDataService.Received(1).GetBarData(_mainSymbol);
			_stocksRepository.DidNotReceiveWithAnyArgs().All();
		}

		[Test]
		public void should_except_symbols_in_position()
		{
			// arrange
			var stocks = new List<Stock>()
			{
				new Stock {Id =1, Name = "SomeSymbol1",Size = 1,Symbol = "1"},
				new Stock {Id =1, Name = "SomeSymbol2",Size = 1,Symbol = "2"},
				new Stock {Id =1, Name = "SomeSymbol3",Size = 1,Symbol = "3"},
				new Stock {Id =1, Name = "SomeSymbol4",Size = 1,Symbol = "4"},
			};
			_strategyRulesChecker.IsSAndP500IndexAbove200Ma(Arg.Any<double[]>())
				.Returns(true);
			_stocksRepository.GetActualStocks()
				.Returns(stocks);
			_strategyRulesChecker.IsStockAboveMa(Arg.Any<double[]>(), Arg.Any<int>())
				.Returns(true);
			_positionsService.GetAllPositions()
				.Returns(
					a =>
						Task<IEnumerable<PositionInfo>>.Factory.StartNew(() => new List<PositionInfo> { new PositionInfo { Symbol = "3" } }));
			//act
			_sut.RunOnce();

			//assert

			_rankingService.Received(1).Rank(
					Arg.Is<IEnumerable<string>>(x => x.All(a => a != "3")));
		}

		[Test]
		public void should_work_only_with_top_rank_data()
		{
			// arrange
			var stocks = new List<Stock>()
			{
				new Stock {Id =1, Name = "SomeSymbol1",Size = 1,Symbol = "1"},
				new Stock {Id =1, Name = "SomeSymbol2",Size = 1,Symbol = "2"},
				new Stock {Id =1, Name = "SomeSymbol3",Size = 1,Symbol = "3"},
				new Stock {Id =1, Name = "SomeSymbol4",Size = 1,Symbol = "4"},
			};
			_strategyRulesChecker.IsSAndP500IndexAbove200Ma(Arg.Any<double[]>())
				.Returns(true);
			_stocksRepository.GetActualStocks()
				.Returns(stocks);
			_rankingService.Rank(
					Arg.Is<IEnumerable<string>>(x => x.Intersect(stocks.Select(a => a.Symbol)).Count() == 4))
				.Returns(stocks.Select((s, i) => new RankingResult
				{
					BarData = new List<Bar>(),
					Symbol = s.Symbol,
					Rank = i
				}));
			_strategyRulesChecker.IsStockAboveMa(Arg.Any<double[]>(), Arg.Any<int>())
				.Returns(true);
			//act
			_sut.RunOnce();

			//assert
			_historicalDataService.Received(1).GetBarData(_mainSymbol);
			_strategyRulesChecker.Received(1).IsSAndP500IndexAbove200Ma(Arg.Any<double[]>());
			_settingsProvider.Received(1).GetAppConfigValue<int>(Consts.ApplicationSettings.TopRankStocksCount);
			_stocksRepository.Received(1).GetActualStocks();
			_strategyRulesChecker.Received(2).IsStockAboveMa(Arg.Any<double[]>(), Arg.Any<int>());
			_strategyRulesChecker.Received(2).HasNotHadGap(Arg.Any<IEnumerable<Bar>>());
		}

		[Test]
		public void should_calculate_amount_by_risk_only_if_stock_above_sma_and_there_are_no_gaps()
		{
			// arrange
			var stocks = new List<Stock>()
			{
				new Stock {Id =1, Name = "SomeSymbol1",Size = 1,Symbol = "1"},
				new Stock {Id =1, Name = "SomeSymbol2",Size = 1,Symbol = "2"},
				new Stock {Id =1, Name = "SomeSymbol3",Size = 1,Symbol = "3"},
				new Stock {Id =1, Name = "SomeSymbol4",Size = 1,Symbol = "4"},
			};

			_strategyRulesChecker.IsSAndP500IndexAbove200Ma(Arg.Any<double[]>())
				.Returns(true);
			_stocksRepository.GetActualStocks()
				.Returns(stocks);
			_rankingService.Rank(
					Arg.Is<IEnumerable<string>>(x => x.Intersect(stocks.Select(a => a.Symbol)).Count() == 4))
				.Returns(stocks.Select((s, i) => new RankingResult
				{
					BarData = new List<Bar>(),
					Symbol = s.Symbol,
					Rank = i
				}));
			_strategyRulesChecker.IsStockAboveMa(Arg.Any<double[]>(), Arg.Any<int>())
				.Returns(true);
			_strategyRulesChecker.HasNotHadGap(Arg.Any<IEnumerable<Bar>>())
				.Returns(true);
			//act
			_sut.RunOnce();

			//assert
			_historicalDataService.Received(1).GetBarData(_mainSymbol);
			_strategyRulesChecker.Received(1).IsSAndP500IndexAbove200Ma(Arg.Any<double[]>());
			_settingsProvider.Received(1).GetAppConfigValue<int>(Consts.ApplicationSettings.TopRankStocksCount);
			_stocksRepository.Received(1).GetActualStocks();
			_strategyRulesChecker.Received(2).IsStockAboveMa(Arg.Any<double[]>(), Arg.Any<int>());
			_strategyRulesChecker.Received(2).HasNotHadGap(Arg.Any<IEnumerable<Bar>>());

			_finalCaculationFactory.Received(1).CreateForBuyStrategy(Arg.Is<IEnumerable<RankingResult>>(a => a.Any(r => r.Symbol == "4") && a.Any(r => r.Symbol == "3")));
		}
	}
}
