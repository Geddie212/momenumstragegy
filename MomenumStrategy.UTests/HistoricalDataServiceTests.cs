﻿using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.EOD.DataSource;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.UTests
{
	[TestFixture]
	public class HistoricalDataServiceTests
	{
		private IHistoricalDataService _historicalDataService;
		private IEodDataProvider _yahooDataProvider2;

		[SetUp]
		public void SetUp()
		{
			_yahooDataProvider2 = Substitute.For<IEodDataProvider>();
			_historicalDataService = new HistoricalDataService(_yahooDataProvider2);
		}

		[Test]
		public void should_return_historical_data()
		{
			_yahooDataProvider2.GetEod("AAA", Arg.Any<DateTime>()).Returns(new List<EodDataModel>()
			{
				new EodDataModel()
				{
					Close = 1,
					Open = 2,
					High = 3,
					Low = 4,
				}
			});

			var result = _historicalDataService.GetBarData("AAA");

			Assert.That(result, Is.Not.Empty);
			var bar = result.Single();
			Assert.That(bar.Close, Is.EqualTo(1));
			Assert.That(bar.Open, Is.EqualTo(2));
			Assert.That(bar.High, Is.EqualTo(3));
			Assert.That(bar.Low, Is.EqualTo(4));
		}
	}
}
