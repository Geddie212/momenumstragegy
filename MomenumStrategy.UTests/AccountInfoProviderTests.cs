﻿using MomenumStrategy.TWS;
using MomenumStrategy.Utils;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MomenumStrategy.UTests
{
	[TestFixture]
	public class AccountInfoProviderTests
	{
		private IAccountInfoProvider _sut;
		private ITwsClient _client;

		[SetUp]
		public void SetUp()
		{
			_client = Substitute.For<ITwsClient>();
			_sut = new AccountInfoProvider(_client);
		}

		[Test]
		public void should_return_zero_if_there_is_account_value()
		{
			_client.GetAccountInto()
				.Returns(
					Task<IEnumerable<AccoutInfo>>.Factory.StartNew(
						o => new List<AccoutInfo> {new AccoutInfo {Value = "", Currency = "USD", Property = Consts.AccountValueKeyName}},
						null));

			var result = _sut.GetAccoutValue();

			Assert.That(result.Result, Is.EqualTo(0));
		}

		[Test]
		public void should_return_approriate_account_value()
		{
			_client.GetAccountInto()
				.Returns(
					Task<IEnumerable<AccoutInfo>>.Factory.StartNew(
						o =>
							new List<AccoutInfo>
							{
								new AccoutInfo {Value = "123.23", Currency = "USD", Property = Consts.AccountValueKeyName},
								new AccoutInfo {Value = "421.23", Currency = "USD", Property = "SomeAnotherProperty"}
							},
						null));

			var result = _sut.GetAccoutValue();

			Assert.That(result.Result, Is.EqualTo(123.23));
		}

		[Test]
		public void should_return_zero_if_there_are_not_account_values_yet()
		{
			_client.GetAccountInto()
				.Returns(
					Task<IEnumerable<AccoutInfo>>.Factory.StartNew(
						o =>
							new List<AccoutInfo>
							{
							},
						null));

			var result = _sut.GetAccoutValue();

			Assert.That(result.Result, Is.EqualTo(0));
		}
	}
}
