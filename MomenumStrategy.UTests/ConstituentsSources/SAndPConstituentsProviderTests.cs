﻿using MomenumStrategy.SAndPConstituentsSource;
using MomenumStrategy.Utils.Contituents;
using NSubstitute;
using NUnit.Framework;
using System.Linq;

namespace MomenumStrategy.UTests.ConstituentsSources
{
	[TestFixture]
	public class SAndPConstituentsProviderTests
	{
		private IIndexConstituentsSource _sut;
		private IDataOkfnClient _client;


		[SetUp]
		public void SetUp()
		{
			_client = Substitute.For<IDataOkfnClient>();
			_sut = new SAndPConstituentsProvider(_client);
		}

		[Test]
		public void should_convert_json()
		{
			_client.LoadAsJson()
				.Returns(
					"[{\"Symbol\":\"MMM\",\"Name\":\"3M Company\",\"Sector\":\"Industrials\"},{\"Symbol\":\"ABT\",\"Name\":\"Abbott Laboratories\",\"Sector\":\"Health Care\"}]");
			var result = _sut.Load().ToList();
			Assert.That(result, Is.Not.Empty);
			Assert.That(result[0].Symbol, Is.EqualTo("MMM"));
			Assert.That(result[0].Name, Is.EqualTo("3M Company"));

			Assert.That(result[1].Symbol, Is.EqualTo("ABT"));
			Assert.That(result[1].Name, Is.EqualTo("Abbott Laboratories"));
		}
	}
}
