﻿using MomenumStrategy.Applications.Serivces.Indicators;
using MomenumStrategy.Applications.Strategy;
using MomenumStrategy.Utils;
using NSubstitute;
using NUnit.Framework;
using System;

namespace MomenumStrategy.UTests
{
	[TestFixture]
	public class StrategyRulesCheckerTests
	{
		private IStrategyRulesChecker _sut;
		private IMovingAverageService _movingAverageService;
		private IGapCalculationService _gapCalculationService;
		private ISettingsProvider _settingsProvider;
		[SetUp]
		public void SetUp()
		{
			_movingAverageService = Substitute.For<IMovingAverageService>();
			_gapCalculationService = Substitute.For<IGapCalculationService>();
			_settingsProvider = Substitute.For<ISettingsProvider>();
			_sut = new StrategyRulesChecker(_movingAverageService, _gapCalculationService, _settingsProvider);
		}

		[Test]
		public void should_check_if_it_is_wednesday_today()
		{
			using (DateTimeProvider.FreezeTime(new DateTime(2016,09,07)))
			{
				var result = _sut.IsItWednesday();
				Assert.That(result,Is.True);
			}
		}

		[Test]
		public void should_return_true_if_s_and_p_above_ma()
		{
			var data = new double[] {0, 0, 1, 2, 5};
			_movingAverageService.MovingAverageCalculator(data, 200)
				.Returns(a => new double[] {0, 0, 1, 2, 4});
			var result = _sut.IsSAndP500IndexAbove200Ma(data);
				Assert.That(result,Is.True);
		}

		[Test]
		public void should_return_false_if_s_and_p_below_ma()
		{
			var data = new double[] {0, 0, 1, 2, 4};
			_movingAverageService.MovingAverageCalculator(data, 200)
				.Returns(a => new double[] { 0, 0, 1, 2, 5 });
			var result = _sut.IsSAndP500IndexAbove200Ma(data);
			Assert.That(result, Is.False);
		}

		[Test]
		public void should_return_true_is_symbol_above_ma()
		{
			var data = new double[] { 0, 0, 1, 2, 5 };
			_movingAverageService.MovingAverageCalculator(data, 5)
				.Returns(a => new double[] { 0, 0, 1, 2, 4 });
			var result = _sut.IsStockAboveMa(data, 5);
			Assert.That(result, Is.True);
		}

		[Test]
		public void should_return_false_is_symbol_below_ma()
		{
			var data = new double[] { 0, 0, 1, 2, 4 };
			_movingAverageService.MovingAverageCalculator(data, 5)
				.Returns(a => new double[] { 0, 0, 1, 2, 5 });
			var result = _sut.IsStockAboveMa(data, 5);
			Assert.That(result, Is.False);
		}
	}
}
