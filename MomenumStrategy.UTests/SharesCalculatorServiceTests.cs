﻿using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Applications.Serivces.Indicators;
using MomenumStrategy.TWS;
using MomenumStrategy.Utils;
using MomenumStrategy.Utils.Factories;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MomenumStrategy.UTests
{
	[TestFixture]
	public class SharesCalculatorServiceTests
	{
		private ISharesCalculatorService _sharesCalculatorService;
		private IAccountInfoProvider _accountInfoProvider;
		private ISettingsProvider _settingsProvider;
		private IAtrService _atrService;
		private ILogMessager _logMessager;

		[SetUp]
		public void SetUp()
		{
			_accountInfoProvider = Substitute.For<IAccountInfoProvider>();
			_settingsProvider = Substitute.For<ISettingsProvider>();
			_atrService = Substitute.For<IAtrService>();
			_logMessager = Substitute.For<ILogMessager>();
			_sharesCalculatorService = new SharesCalculatorService(_accountInfoProvider, _settingsProvider, _atrService, _logMessager);
		}

		[Test]
		public void should_return_shares_amount()
		{
			var task = Task.Factory.StartNew<double>(
				() => 1000);
			_settingsProvider.GetAppConfigValue<double>(Consts.ApplicationSettings.RiskValue).Returns(0.1);
			_accountInfoProvider.GetAccoutValue()
				.Returns(task);
			_atrService.AtrCalculator(Arg.Any<double[]>(), Arg.Any<double[]>(), Arg.Any<double[]>(), Arg.Any<int>())
				.Returns(a => new double[] { 1, 2, 3, 4, 5 });
			var result = _sharesCalculatorService.CalculateAmountByRisk(new List<Bar>(), "AAA");

			Assert.That(result.Result, Is.EqualTo(20));
		}

		[Test]
		public void should_return_zero_shares_amount_when_account_value_is_zero()
		{
			var task = Task.Factory.StartNew<double>(
				() => 0);
			_accountInfoProvider.GetAccoutValue()
				.Returns(task);
			_atrService.ClearReceivedCalls();

			var result = _sharesCalculatorService.CalculateAmountByRisk(new List<Bar>(), "AAA");

			Assert.That(result.Result, Is.EqualTo(0));
			_atrService
				.Received(0).AtrCalculator(Arg.Any<double[]>(), Arg.Any<double[]>(), Arg.Any<double[]>(), Arg.Any<int>());
			_settingsProvider.Received(0).GetAppConfigValue<double>(Consts.ApplicationSettings.RiskValue);
		}
	}
}
