﻿using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Applications.Serivces.Indicators;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace MomenumStrategy.UTests.Indicators
{
	[TestFixture]
	public class GapCalculationServiceTests
	{
		private IGapCalculationService _sut;

		[SetUp]
		public void SetUp()
		{
			_sut = new GapCalculationService();
		}

		[Test]
		public void shold_calc_grow_gap()
		{
			var data = new List<Bar> {new Bar {Open = 8.0d, Close = 10.0d}, new Bar {Open = 9.0d, Close = 12.0d}};
			var result = _sut.GetGapPercentage(data);
			Assert.That(Math.Round(result), Is.EqualTo(10.0d));
		}

		[Test]
		public void shold_calc_crash_gap()
		{
			var data = new List<Bar> { new Bar { Open = 8.0d, Close = 9.0d }, new Bar { Open = 10.0d, Close = 12.0d } };
			var result = _sut.GetGapPercentage(data);
			Assert.That(Math.Round(result), Is.EqualTo(10.0d));
		}

		[Test]
		public void shold_return_max_gap()
		{
			var data = new List<Bar> {
				new Bar { Open = 8.0d, Close = 10.0d },
				new Bar { Open = 9.0d, Close = 12.0d },
				new Bar { Open = 12.1d, Close = 10.0d },
				new Bar { Open = 8.0d, Close = 12.0d } };
			var result = _sut.GetGapPercentage(data);
			Assert.That(Math.Round(result), Is.EqualTo(20.0d));
		}
	}
}
