﻿using MomenumStrategy.Applications.Serivces.Indicators;
using MomenumStrategy.Utils.Exceptions;
using NUnit.Framework;

namespace MomenumStrategy.UTests.Indicators
{
	[TestFixture]
	public class LinearRegressionSlopeSerivceTests
	{
		private ILinearRegressionSlopeSerivce _sut;

		[SetUp]
		public void SetUp()
		{
			_sut = new LinearRegressionSlopeSerivce();
		}

		[Test]
		public void should_calc_slope()
		{
			var input = new double[] { 1, 10, 20, 30, 50 };
			var output = _sut.GetSlope(input, 2);
			Assert.That(output.Length, Is.EqualTo(5));
		}

		[Test]
		public void should_throw_exception_if_not_enough_data()
		{
			Assert.Throws<NotEnoughDataException>(() =>
			{
				var input = new double[] { 1, 2, 3, 4, 5 };
				_sut.GetSlope(input, 7);
			});
		}
	}
}
