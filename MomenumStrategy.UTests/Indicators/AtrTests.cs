﻿using MomenumStrategy.Applications.Serivces.Indicators;
using NUnit.Framework;

namespace MomenumStrategy.UTests.Indicators
{
	[TestFixture]
	public class AtrTests
	{
		private IAtrService _sut;

		[SetUp]
		public void SetUp()
		{
			_sut = new AtrService();
		}

		[Test]
		public void should_calc_atr()
		{
			var close = new double[] { 5, 4, 3, 2, 1 };
			var high = new double[] { 5, 6, 7, 8, 9 };
			var low = new double[] { 1, 1, 1, 1, 1 };
			var output = _sut.AtrCalculator(high, low, close, 4);
			Assert.That(output.Length, Is.EqualTo(5));
			Assert.That(output[0], Is.EqualTo(0));
			Assert.That(output[1], Is.EqualTo(0));
			Assert.That(output[2], Is.EqualTo(0));
			Assert.That(output[3], Is.EqualTo(0));
			Assert.That(output[4], Is.EqualTo(6.5));
		}
	}
}
