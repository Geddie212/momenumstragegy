﻿using MomenumStrategy.Applications.Serivces.Indicators;
using MomenumStrategy.Utils.Exceptions;
using NUnit.Framework;

namespace MomenumStrategy.UTests.Indicators
{
	[TestFixture]
	public class SmaTests
	{
		private IMovingAverageService _sut;

		[SetUp]
		public void SetUp()
		{
			_sut = new MovingAverageService();
		}

		[Test]
		public void should_calc_sma()
		{
			var input = new double[] { 1, 2, 3, 4, 5 };
			var output = _sut.MovingAverageCalculator(input, 2);
			Assert.That(output.Length, Is.EqualTo(5));
			Assert.That(output[0], Is.EqualTo(0));
			Assert.That(output[1], Is.EqualTo(1.5));
			Assert.That(output[2], Is.EqualTo(2.5));
			Assert.That(output[3], Is.EqualTo(3.5));
			Assert.That(output[4], Is.EqualTo(4.5));
		}

		[Test]
		public void should_throw_exception_if_not_enough_data()
		{
			Assert.Throws<NotEnoughDataException>(() =>
			{
				var input = new double[] {1, 2, 3, 4, 5};
				_sut.MovingAverageCalculator(input, 7);
			});
		}
	}
}
