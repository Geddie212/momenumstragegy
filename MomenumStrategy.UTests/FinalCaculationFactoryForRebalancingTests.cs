﻿using MomenumStrategy.Applications.Factories;
using MomenumStrategy.Applications.Models;
using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.TWS;
using MomenumStrategy.Utils.Factories;
using MomenumStrategy.YF;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MomenumStrategy.UTests
{
	[TestFixture]
	public class FinalCaculationFactoryForRebalancingTests
	{
		private IFinalCaculationFactory _sut;
		private ISharesCalculatorService _sharesCalculatorService;
		private IAccountInfoProvider _accountInfoProvider;
		private ILogMessager _logMessager;
		private IRebalancingCalculator _rebalancingCalculator;
		private IList<SymbolRebalancingInfo> _symbolRebalancingInfos;
		private IYahooDataProvider2 _yahooDataProvider2;
		private IPositionsService _positionsService;

		[SetUp]
		public void SetUp()
		{
			_sharesCalculatorService = Substitute.For<ISharesCalculatorService>();
			_accountInfoProvider = Substitute.For<IAccountInfoProvider>();
			_logMessager = Substitute.For<ILogMessager>();
			_rebalancingCalculator = Substitute.For<IRebalancingCalculator>();
			_yahooDataProvider2 = Substitute.For<IYahooDataProvider2>();
			_positionsService = Substitute.For<IPositionsService>();
			_sut = new FinalCaculationFactory(_sharesCalculatorService, _accountInfoProvider, _logMessager,
				_rebalancingCalculator);
			_accountInfoProvider.GetAccoutValue().Returns(Task.Factory.StartNew(() => double.MaxValue));
			_symbolRebalancingInfos = new List<SymbolRebalancingInfo>
			{
				new SymbolRebalancingInfo {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "A", Rank = 3, Amount = 1, Action = RebalancingAction.Rebalance},
				new SymbolRebalancingInfo {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "B", Rank = 1, Amount = 1, Action = RebalancingAction.Close},
				new SymbolRebalancingInfo {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "C", Rank = 2, Amount = 1, Action = RebalancingAction.Rebalance},
			};
		}



		[Test]
		public void should_not_buy_during_the_rebalancing_if_nothing_to_buy()
		{
			// arrange
			_rebalancingCalculator.GetAdditionAmountToBuy("A").Returns(a => Task.Factory.StartNew(() => 0));
			_rebalancingCalculator.GetAdditionAmountToBuy("C").Returns(a => Task.Factory.StartNew(() => 1));

			//act
			var result = _sut.CreateForRebalancing(_symbolRebalancingInfos).Result;

			//assert
			_logMessager.Received(1).SendDebugEvent(Arg.Is<string>(a => a.Contains("been skipped because canculated amount of shares is 0")));
			Assert.That(result.Count(a => a.BuyOrSell == OrderType.Buy), Is.EqualTo(1));
			Assert.That(result.First(a => a.BuyOrSell == OrderType.Buy).Symbol, Is.EqualTo("C"));
		}

		[Test]
		public void should_not_buy_becuase_total_amount_has_been_exeeded()
		{
			// arrange
			_accountInfoProvider.GetAccoutValue().Returns(Task.Factory.StartNew(() => 10.0));
			_symbolRebalancingInfos = new List<SymbolRebalancingInfo>
			{
				new SymbolRebalancingInfo {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "A", Rank = 3, Amount = 5, Action = RebalancingAction.Rebalance},
				new SymbolRebalancingInfo {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "B", Rank = 1, Amount = 5, Action = RebalancingAction.Rebalance},
			};
			_rebalancingCalculator.GetAdditionAmountToBuy("A").Returns(a => Task.Factory.StartNew(() => 1));
			_rebalancingCalculator.GetAdditionAmountToBuy("B").Returns(a => Task.Factory.StartNew(() => 1));

			//act
			var result = _sut.CreateForRebalancing(_symbolRebalancingInfos).Result;

			//assert
			_logMessager.Received(2).SendDebugEvent(Arg.Is<string>(a => a.Contains("has been skipped because the total sum ")));
			Assert.That(result.Count(a => a.BuyOrSell == OrderType.Buy), Is.EqualTo(0));
		}
		[Test]
		public void should_not_buy_becuase_total_amount_has_been_exeeded_and_if_something_has_to_be_sold_but_not_enougth()
		{
			// arrange
			_accountInfoProvider.GetAccoutValue().Returns(Task.Factory.StartNew(() => 12.0));
			_symbolRebalancingInfos = new List<SymbolRebalancingInfo>
			{
				new SymbolRebalancingInfo {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "A", Rank = 3, Amount = 5, Action = RebalancingAction.Rebalance},
				new SymbolRebalancingInfo {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "B", Rank = 1, Amount = 5, Action = RebalancingAction.Rebalance},
				new SymbolRebalancingInfo {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "C", Rank = 1, Amount = 2, Action = RebalancingAction.Close},
			};
			_rebalancingCalculator.GetAdditionAmountToBuy("A").Returns(a => Task.Factory.StartNew(() => 4));
			_rebalancingCalculator.GetAdditionAmountToBuy("B").Returns(a => Task.Factory.StartNew(() => 1));
			_rebalancingCalculator.GetAdditionAmountToBuy("C").Throws(new Exception("fuck you!"));

			//act
			var result = _sut.CreateForRebalancing(_symbolRebalancingInfos).Result;

			//assert
			_logMessager.Received(1).SendDebugEvent(Arg.Is<string>(a => a.Contains("has been skipped because the total sum ")));
			Assert.That(result.Count(a => a.BuyOrSell == OrderType.Buy), Is.EqualTo(1));
			Assert.That(result.First(a => a.BuyOrSell == OrderType.Buy).Symbol, Is.EqualTo("B"));
		}


		[Test]
		public void should_buy_becuase_total_amount_has_been_exeeded_but_we_have_something_to_sell()
		{
			// arrange
			_accountInfoProvider.GetAccoutValue().Returns(Task.Factory.StartNew(() => 15.0));
			_symbolRebalancingInfos = new List<SymbolRebalancingInfo>
			{
				new SymbolRebalancingInfo {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "B", Rank = 1, Amount = 4, Action = RebalancingAction.Rebalance},
				new SymbolRebalancingInfo {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "A", Rank = 3, Amount = 4, Action = RebalancingAction.Rebalance},
				new SymbolRebalancingInfo {BarData = new List<Bar> {new Bar {Close = 1} }, Symbol = "C", Rank = 1, Amount = 7, Action = RebalancingAction.Close},
			};
			_rebalancingCalculator.GetAdditionAmountToBuy("A").Returns(a => Task.Factory.StartNew(() => 4));
			_rebalancingCalculator.GetAdditionAmountToBuy("B").Returns(a => Task.Factory.StartNew(() => 3));
			_rebalancingCalculator.GetAdditionAmountToBuy("C").Throws(new Exception("fuck you!"));

			//act
			var result = _sut.CreateForRebalancing(_symbolRebalancingInfos).Result;

			//assert
			_logMessager.Received(0).SendDebugEvent(Arg.Any<string>());
			Assert.That(result.Count(a => a.BuyOrSell == OrderType.Buy), Is.EqualTo(2));
			Assert.That(result.First(a => a.BuyOrSell == OrderType.Buy).Symbol, Is.EqualTo("A"));
			Assert.That(result.Last(a => a.BuyOrSell == OrderType.Buy).Symbol, Is.EqualTo("B"));
		}
	}
}
