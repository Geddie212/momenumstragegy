﻿using Emesary;
using System;

namespace MomenumStrategy.Utils.Notifications
{
	public class LogNotification : INotification
	{
		public object Value { get; set; }

		public DateTime DateTime { get; private set; }

		public LogNotification()
		{
			DateTime = DateTime.Now;
		}

		public string Message { get; set; }
		public LogNotificationCategory Category { get; set; }
	}
}
