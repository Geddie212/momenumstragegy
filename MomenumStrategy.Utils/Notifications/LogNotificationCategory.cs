﻿namespace MomenumStrategy.Utils.Notifications
{
	public enum LogNotificationCategory
	{
		Error,
		Warning,
		Success,
		Debug
	}
}
