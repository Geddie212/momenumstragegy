﻿using System;

namespace MomenumStrategy.Utils
{
	public static class DateTimeProvider
	{

		private static DateTime? _frozenTime;

		public static DateTime Now
		{
			get { return _frozenTime ?? DateTime.Now; }
		}

		public static DateTime NowTruncated
		{
			get
			{
				return _frozenTime.HasValue
						   ? new DateTime(_frozenTime.Value.Year, _frozenTime.Value.Month, _frozenTime.Value.Day,
										  _frozenTime.Value.Hour, _frozenTime.Value.Minute, _frozenTime.Value.Second)
						   : DateTime.Now;
			}
		}

		public static DateTime Today
		{
			get { return Now.Date; }
		}

		public static IDisposable FreezeTime(DateTime theFrozenTime)
		{
			var currentFrozenTime = _frozenTime;
			_frozenTime = theFrozenTime;
			return new DisposableAction<object>(a => _frozenTime = currentFrozenTime);
		}

		public static bool IsFrozen()
		{
			return _frozenTime.HasValue;
		}

		public static void UnFreezeTime()
		{
			_frozenTime = null;
		}

		//extensions
		public static DateTime ToLocalTimeZone(this DateTime dt, string timeZoneId)
		{
			return !string.IsNullOrEmpty(timeZoneId)
				? TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dt.ToUniversalTime(), timeZoneId)
				: dt;
		}


		public class DisposableAction<T> : IDisposable where T : class 
		{
			readonly Action<T> action;

			/// <summary>
			/// Initializes a new instance of the <see cref="DisposableAction"/> class.
			/// </summary>
			/// <param name="action">The action to execute on dispose</param>
			public DisposableAction(Action<T> action)
			{
				if (action == null)
					throw new ArgumentNullException("action");
				this.action = action;
			}

			/// <summary>
			/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
			/// </summary>
			public virtual void Dispose()
			{
				action(null);
			}
		}
	}
}
