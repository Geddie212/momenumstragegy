﻿using Emesary;
using MomenumStrategy.Utils.Notifications;
using System.Threading.Tasks;

namespace MomenumStrategy.Utils.Factories
{
	public interface ILogMessager
	{
		void SendSuccessEvent(string message);
		void SendDebugEvent(string message);
		void SendWarningEvent(string message);
		void SendErrorEvent(string message);
	}

	public class LogMessager : ILogMessager
	{
		public void SendSuccessEvent(string message)
		{
			Task.Factory.StartNew(() => GlobalTransmitter.NotifyAll(new LogNotification
			{
				Value = Consts.NotificationTypes.LogIt,
				Message = message,
				Category = LogNotificationCategory.Success
			}));
		}

		public void SendDebugEvent(string message)
		{
			Task.Factory.StartNew(() => GlobalTransmitter.NotifyAll(new LogNotification
			{
				Value = Consts.NotificationTypes.LogIt,
				Message = message,
				Category = LogNotificationCategory.Debug
			}));
		}

		public void SendWarningEvent(string message)
		{
			Task.Factory.StartNew(() => GlobalTransmitter.NotifyAll(new LogNotification
			{
				Value = Consts.NotificationTypes.LogIt,
				Message = message,
				Category = LogNotificationCategory.Warning
			}));
		}

		public void SendErrorEvent(string message)
		{
			Task.Factory.StartNew(() => GlobalTransmitter.NotifyAll(new LogNotification
			{
				Value = Consts.NotificationTypes.LogIt,
				Message = message,
				Category = LogNotificationCategory.Error
			}));
		}
	}
}
