﻿namespace MomenumStrategy.Utils
{
	public interface IFtsEntity
	{
		bool Fit(string searchString);
	}
}
