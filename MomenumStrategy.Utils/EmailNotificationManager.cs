﻿using Emesary;
using MomenumStrategy.Utils.Notifications;
using System.Threading.Tasks;

namespace MomenumStrategy.Utils
{
	public interface IEmailNotificationManager
	{
		Task SendEmailNotification(string theme, string message);
	}

	public sealed class EmailNotificationManager : IEmailNotificationManager, IReceiver
	{
		private readonly IEmailService _emailService;

		public EmailNotificationManager(IEmailService emailService)
		{
			_emailService = emailService;
			GlobalTransmitter.Register(this);
		}

		public async Task SendEmailNotification(string theme, string message)
		{
			await _emailService.SendEmailAsync(message, theme, true);
		}

		public ReceiptStatus Receive(INotification message)
		{
			if (message.Value.ToString() == Consts.NotificationTypes.LogIt)
			{
				try
				{

					var logMessage = (LogNotification)message;
					switch (logMessage.Category)
					{
						case LogNotificationCategory.Error:
							_emailService.SendEmailAsync(logMessage.Message, "ERROR", true);
							break;
						case LogNotificationCategory.Warning:
							_emailService.SendEmailAsync(logMessage.Message, "WARNING", false);
							break;
						case LogNotificationCategory.Success:
							break;
						case LogNotificationCategory.Debug:
							break;
					}
				}
				catch (System.Exception exc)
				{

					throw;
				}
			}

			return ReceiptStatus.OK;
		}
	}
}
