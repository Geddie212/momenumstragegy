﻿using System;
using System.Configuration;
using System.Globalization;

namespace MomenumStrategy.Utils
{
	public interface ISettingsProvider
	{
		T GetAppConfigValue<T>(string name) where T : IConvertible;
	}
	public class SettingsProvider : ISettingsProvider
	{
		public T GetAppConfigValue<T>(string name) where T : IConvertible
		{
			return (T) Convert.ChangeType(ConfigurationManager.AppSettings[name], typeof(T), CultureInfo.InvariantCulture);
		}
	}
}
