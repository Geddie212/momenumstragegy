﻿using NLog;
using System;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MomenumStrategy.Utils
{
	public interface IEmailService
	{
		Task SendEmailAsync(string message, string theme, bool isImportant);
	}

	public class EmailService : IEmailService
	{
		private readonly ISettingsProvider _settingsProvider;
		private readonly ILogger _logger;

		public EmailService(ISettingsProvider settingsProvider)
		{
			_settingsProvider = settingsProvider;
			_logger = LogManager.GetLogger(this.GetType().FullName);
		}

		public async Task SendEmailAsync(string message, string theme, bool isImportant)
		{
			await Task.Factory.StartNew(() =>
			{
				try
				{
					using (var msg = new MailMessage
					{
						From = new MailAddress(_settingsProvider.GetAppConfigValue<string>(Consts.ApplicationSettings.GmailNotificationEmail)),
						Subject = theme,
						Body = message
					})
					{
						foreach (var source in _settingsProvider.GetAppConfigValue<string>(Consts.ApplicationSettings.NotificationRecepients).Split(new char[] { ';' }, StringSplitOptions.None).Select(a => new MailAddress(a)).ToList())
						{
							msg.To.Add(source);
						}
						using (var client = new SmtpClient("smtp.mail.yahoo.com", 587)
						{
							UseDefaultCredentials = false,
							Credentials = new System.Net.NetworkCredential(
								_settingsProvider.GetAppConfigValue<string>(Consts.ApplicationSettings.GmailNotificationEmail),
								_settingsProvider.GetAppConfigValue<string>(Consts.ApplicationSettings.GmailNotificationPwd)),
							EnableSsl = true
						})
						{
							client.Send(msg);
						}
					}
					
				}
				catch (Exception exc)
				{
					_logger.Fatal(exc);
					//throw;
				}
			});
		}
	}
}
