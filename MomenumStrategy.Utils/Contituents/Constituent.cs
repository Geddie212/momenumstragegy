﻿namespace MomenumStrategy.Utils.Contituents
{
	public class Constituent
	{
		public string Symbol { get; private set; }
		public string Name { get; private set; }

		public Constituent(string symbol, string name)
		{
			Symbol = symbol;
			Name = name;
		}
	}
}
