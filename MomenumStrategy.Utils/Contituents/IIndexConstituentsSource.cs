﻿using System.Collections.Generic;

namespace MomenumStrategy.Utils.Contituents
{
	public interface IIndexConstituentsSource
	{
		IEnumerable<Constituent> Load();
	}

}
