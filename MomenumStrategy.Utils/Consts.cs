﻿namespace MomenumStrategy.Utils
{
	public static class Consts
	{
		public const string AccountValueKeyName = "NetLiquidation";

		public static class ApplicationSettings
		{
			public const string RunUpdateStockTimePeriodInDays = "RunUpdateStockTimePeriodInDays";
			public const string NotificationRecepients = "NotificationRecepients";
			public const string GmailNotificationEmail = "GmailNotificationEmail";
			public const string GmailNotificationPwd = "GmailNotificationPwd";
			public const string RunStrategyPeriodInSeconds = "RunStrategyPeriodInSeconds";
			public const string StockGepThresholdInPercent = "StockGepThresholdInPercent";
			public const string TopRankStocksCount = "TopRankStocksCount";
			public const string MainIndex = "MainIndex";
			public const string RiskValue = "RiskValue";
			public const string RebalancingShareAmountThreshold = "RebalancingShareAmountThreshold";
			public const string StockCurrency = "StockCurrency";
			public const string DisableTheRuleIfItsWednesday = "DisableTheRuleIfItsWednesday";
		}

		public static class  NotificationTypes
		{
			public const string LogIt = "LogIt";
			public const string PositionsUpdated = "PositionsUpdated";
			public const string OrdersUpdated = "PositionsUpdated";
		}

		public static class GlobalParamnames
		{
			public const string IsMainSymbolAbove = "IsMainSymbolAbove";
		}
	}
}
