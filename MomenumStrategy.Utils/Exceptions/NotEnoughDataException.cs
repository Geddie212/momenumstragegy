﻿using System;

namespace MomenumStrategy.Utils.Exceptions
{
	public sealed class NotEnoughDataException : Exception
	{
		public NotEnoughDataException(string notEnoughtFor, object current, object expected)
			: base($"There is not enough data for [{notEnoughtFor}], expected:[{current}], actual:[{expected}]")
		{

		}
	}
}
