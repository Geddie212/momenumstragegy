﻿using MomenumStrategy.Utils;
using MomenumStrategy.YF.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using YahooFinance.NET;

namespace MomenumStrategy.YF
{
	public interface IYahooDataProvider2
	{
		IEnumerable<HistoricalPriceData> GetDailyPriceHistoryByNow(string symbol, DateTime @from);
		IDictionary<string, double> GetActualPrices();
	}

	public class YahooDataProvider2 : IYahooDataProvider2
	{
		private readonly YahooFinanceClient _yahooFinance;
		private readonly IYahooStreaming _yahooStreaming;
		private readonly string[] _exchanges =
		{
			"TSE",
			"CVE",
			"AMEX",
			"NASDAQ",
			"NYSE",
		};
		string crumb = "O4f3gZJOaY6";
		string cookie = "7epiu65c6cp3l&b=3&s=sl";

		public YahooDataProvider2(IYahooStreaming yahooStreaming)
		{
			_yahooStreaming = yahooStreaming;
			_yahooFinance = new YahooFinanceClient(cookie, crumb);
		}

		public IEnumerable<HistoricalPriceData> GetDailyPriceHistoryByNow(string symbol, DateTime @from)
		{

			List<YahooHistoricalPriceData> yahooPriceHistory = _yahooFinance.GetDailyHistoricalPriceData(symbol, @from, DateTimeProvider.Now);
			return yahooPriceHistory.Select(a => new HistoricalPriceData
			{
				Date = a.Date,
				AdjClose = a.AdjClose,
				Close = a.Close,
				High = a.High,
				Low = a.Low,
				Open = a.Open,
				Volume = a.Volume,
			});
			//var exceptions = new List<Exception>();
			//foreach (var exchange in _exchanges)
			//{
			//	try
			//	{
			//		var code = _yahooFinance.GetYahooStockCode(exchange, symbol);
			//		if (result.Any())
			//			return result;
			//		throw new Exception($"not data for {exchange}.{symbol} = {code}");
			//	}
			//	catch (Exception exc)
			//	{
			//		exceptions.Add(exc);
			//	}
			//}

			//if (exceptions.Any())
			//{
			//	throw new AggregateException("One of more exception occured during take EOD data", exceptions);
			//}

			//return new HistoricalPriceData[0];
		}

		public IDictionary<string, double> GetActualPrices()
		{
			return _yahooStreaming.GetActualPrices();
		}
	}
}
