﻿using MomenumStrategy.YF.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;

namespace MomenumStrategy.YF
{
	public interface IGoogleDataProvider : IYahooDataProvider2
	{

	}

	public class GoogleDataProvider : IGoogleDataProvider
	{
		public GoogleDataProvider()
		{
		}

		public IEnumerable<HistoricalPriceData> GetDailyPriceHistoryByNow(string symbol, DateTime @from)
		{
			using (var client = new HttpClient())
			{
				var task = client.GetStringAsync($"https://www.google.com/finance/historical?output=csv&q={symbol}");
				task.Wait();
				foreach (var strings in task.Result.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Skip(1))
				{
					var values = strings.Split(new char[] { ',' });
					yield return new HistoricalPriceData()
					{
						Date = Convert.ToDateTime(values[0], CultureInfo.InvariantCulture),
						Open = Convert.ToDecimal(values[1], CultureInfo.InvariantCulture),
						High = Convert.ToDecimal(values[2], CultureInfo.InvariantCulture),
						Low = Convert.ToDecimal(values[3], CultureInfo.InvariantCulture),
						Close = Convert.ToDecimal(values[4], CultureInfo.InvariantCulture),
						Volume = Convert.ToInt64(values[5], CultureInfo.InvariantCulture),
					};
				}
			}
			//List<YahooHistoricalPriceData> yahooPriceHistory = _yahooFinance.GetDailyHistoricalPriceData(symbol, @from, DateTimeProvider.Now);
			//return yahooPriceHistory.Select(a => new HistoricalPriceData()
			//{
			//	Date = a.Date,
			//	AdjClose = a.AdjClose,
			//	Close = a.Close,
			//	High = a.High,
			//	Low = a.Low,
			//	Open = a.Open,
			//	Volume = a.Volume,
			//});
		}

		public IDictionary<string, double> GetActualPrices()
		{
			throw new NotImplementedException();
		}
	}
}
