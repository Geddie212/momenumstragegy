﻿using MomenumStrategy.YF.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using YSQ.core.Historical;

namespace MomenumStrategy.YF
{
	public interface IYahooDataProvider
	{
		IEnumerable<FinanceDataItem> GetDailyData(DateTime from, DateTime to, string symbol);
	}
	public class YahooDataProvider : IYahooDataProvider
	{
		public IEnumerable<FinanceDataItem> GetDailyData(DateTime @from, DateTime to, string symbol)
		{
			//Create the historical price service
			var historical_price_service = new HistoricalPriceService();

			//Get the historical prices
			var historical_prices = historical_price_service.Get(symbol, @from, to, Period.Daily);

			//Use the prices!
			//foreach (var price in historical_prices)
			//{
			//	Console.WriteLine("{0} - {1} ", price.Date.ToString("MMM dd,yyyy"), price.Price);
			//}

			return historical_prices.Select(a=>new FinanceDataItem
			{
				Date = a.Date,
				Price = a.Price
			});
		}
	}
}
