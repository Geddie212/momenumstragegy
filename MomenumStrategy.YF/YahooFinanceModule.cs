﻿using Microsoft.Practices.Unity;

namespace MomenumStrategy.YF
{
	public class YahooFinanceModule
	{
		public void Register(IUnityContainer unityContainer)
		{
			unityContainer.RegisterType<IYahooDataProvider, YahooDataProvider>();
			unityContainer.RegisterType<IYahooDataProvider2, YahooDataProvider2>(new ContainerControlledLifetimeManager());
			unityContainer.RegisterType<IGoogleDataProvider, GoogleDataProvider>(new ContainerControlledLifetimeManager());
			unityContainer.RegisterType<IYahooStreaming, YahooStreaming>(new ContainerControlledLifetimeManager());

		}
	}
}
