﻿using System;

namespace MomenumStrategy.YF.Entities
{
	public class FinanceDataItem
	{
		public DateTime Date { get; set; }
		public decimal Price { get; set; }
	}
}
