﻿using System;

namespace MomenumStrategy.YF.Entities
{
	public class HistoricalPriceData
	{
		public DateTime Date { get; set; }

		public Decimal Open { get; set; }

		public Decimal High { get; set; }

		public Decimal Low { get; set; }

		public Decimal Close { get; set; }

		public long Volume { get; set; }

		public Decimal AdjClose { get; set; }
	}
}
