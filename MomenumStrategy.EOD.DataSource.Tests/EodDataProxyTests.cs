﻿using NUnit.Framework;
using System;
using System.Linq;

namespace MomenumStrategy.EOD.DataSource.Tests
{
	[TestFixture]
	public class EodDataProxyTests
	{
		private IEodDataProxy _sut;

		[SetUp]
		public void SetUp()
		{
			_sut = new EodDataProxy();
		}

		[Test]
		public void should_return_exchanges()
		{
			var result = _sut.GetExchanges();

			CollectionAssert.IsNotEmpty(result);
			var gi = result.Where(a => a.Name.Contains("Global")).ToList();
			CollectionAssert.IsNotEmpty(gi);

		}

		[Test]
		public void should_return_eod_data_NYSE()
		{
			var result = _sut.GetEod("NYSE", "MMM", DateTime.Now.AddYears(-1));

			CollectionAssert.IsNotEmpty(result);
			Assert.That(result.Count(), Is.GreaterThan(90));
		}

		[Test]
		public void should_return_eod_data_SPXT()
		{
			var result = _sut.GetEod("INDEX", "SPXT", DateTime.Now.AddYears(-1));

			CollectionAssert.IsNotEmpty(result);
			Assert.That(result.Count(), Is.GreaterThan(90));
		}

		[Test]
		public void should_return_eod_data_KMI()
		{
			var result = _sut.GetEod("NYSE", "KMI", DateTime.Now.AddYears(-1));

			CollectionAssert.IsNotEmpty(result);
			Assert.That(result.Count(), Is.GreaterThan(90));
		}

		[Test]
		public void should_not_return_eod_data_NASDAQ_MMM()
		{
			Assert.Throws<InvalidSymbolCodeException>(() => _sut.GetEod("NASDAQ", "MMM", DateTime.Now.AddYears(-1)));
		}
	}
}
