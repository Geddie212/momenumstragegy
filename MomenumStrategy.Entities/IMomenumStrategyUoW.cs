﻿using System.Data.Entity;

namespace MomenumStrategy.Entities
{
	public interface IMomenumStrategyUoW : IUnitOfWork<DataModel>
	{
	}
	public class MomenumStrategyUoW : IMomenumStrategyUoW
	{
		private readonly DataModel _entities;
		private readonly object _syncObject = new object();
		private DbContextTransaction _transaction;

		public MomenumStrategyUoW()
		{
			_entities = new DataModel();
			_entities.Configuration.AutoDetectChangesEnabled = true;
		}

		public void Begin()
		{
			lock (_syncObject)
				if (_transaction == null)
				{
					_transaction = _entities.Database.BeginTransaction();
				}
		}

		public void Commit()
		{
			lock (_syncObject)
				if (_transaction != null)
				{
					_entities.SaveChanges();
					_transaction.Commit();
					_transaction = null;
				}
		}

		public void Rollback()
		{
			lock (_syncObject)
				if (_transaction != null)
				{
					_transaction.Rollback();
					_transaction = null;
				}
		}

		public void Flush()
		{
			_entities.SaveChanges();
		}

		public DataModel Context
		{
			get { return _entities; }
		}

		public void Dispose()
		{
			_entities.Dispose();
		}
	}
}
