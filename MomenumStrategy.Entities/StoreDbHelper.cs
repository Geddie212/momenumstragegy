﻿using Microsoft.Practices.ServiceLocation;
using System.Linq;

namespace MomenumStrategy.Entities
{
	public static class StoreDbHelper<T> where T : class
	{

		public static void Add(T entity)
		{
			ServiceLocator.Current.GetInstance<IMomenumStrategyUoW>().Context.Set<T>().Add(entity);
		}
		public static IQueryable<T> All()
		{
			RefreshAll();
			return ServiceLocator.Current.GetInstance<IMomenumStrategyUoW>().Context.Set<T>();
		}

		public static T Find(object id)
		{
			RefreshAll();
			return ServiceLocator.Current.GetInstance<IMomenumStrategyUoW>().Context.Set<T>().Find(id);
		}

		public static void Delete(T entity)
		{
			ServiceLocator.Current.GetInstance<IMomenumStrategyUoW>().Context.Set<T>().Remove(entity);
		}

		public static void RefreshAll()
		{
			foreach (var entity in ServiceLocator.Current.GetInstance<IMomenumStrategyUoW>().Context.ChangeTracker.Entries())
			{
				entity.Reload();
			}
		}
	}
}
