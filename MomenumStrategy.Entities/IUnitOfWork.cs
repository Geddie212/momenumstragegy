﻿using System;
using System.Data.Entity;

namespace MomenumStrategy.Entities
{
	public interface IUnitOfWork<TContext> : IDisposable where TContext : DbContext
	{
		void Begin();
		void Commit();
		void Rollback();

		void Flush();

		TContext Context { get; }
	}
}
