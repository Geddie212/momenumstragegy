﻿using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.Entities
{
	public interface IStocksRepository
	{
		IEnumerable<Stock> All();
		IEnumerable<Stock> GetActualStocks();

		void Add(Stock stock);
		Stock FindBySymbol(string symbol);
	}
	public class StocksRepository : IStocksRepository
	{
		public IEnumerable<Stock> All()
		{
			return StoreDbHelper<Stock>.All().ToList();
		}

		public IEnumerable<Stock> GetActualStocks()
		{
			return StoreDbHelper<Stock>.All().Where(a=>!a.IsDMark).ToList();
		}

		public void Add(Stock stock)
		{
			StoreDbHelper<Stock>.Add(stock);
		}

		public Stock FindBySymbol(string symbol)
		{
			return StoreDbHelper<Stock>.All().First(a => a.Symbol == symbol);
		}
	}
}
