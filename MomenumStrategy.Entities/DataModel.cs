using MomenumStrategy.Entities.Models;

namespace MomenumStrategy.Entities
{
	using System.Data.Entity;

	public partial class DataModel : DbContext
	{
		public DataModel()
			: base("name=DataModel")
		{
		}

		public virtual DbSet<Stock> Stocks { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Stock>().HasKey(a => a.Id);
			modelBuilder.Entity<GlobalDynamicParam>().HasKey(a => a.Id);
		}
	}
}
