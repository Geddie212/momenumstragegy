﻿using MomenumStrategy.Entities.Models;

namespace MomenumStrategy.Entities
{
	public interface IGlobalDynamicParamRepository
	{
		void AddOrUpdate(string key, string value);
		string FindByKey(string key, string defaultVal = "");
	}

	public class GlobalDynamicParamRepository : IGlobalDynamicParamRepository
	{
		public void AddOrUpdate(string key, string value)
		{
			var par = StoreDbHelper<GlobalDynamicParam>.Find(key);
			if (par != null)
			{
				par.Val = value;
			}
			else
			{
				StoreDbHelper<GlobalDynamicParam>.Add(new GlobalDynamicParam {Id = key, Val = value}); 
			}
		}

		public string FindByKey(string key, string defaultVal = "")
		{
			return StoreDbHelper<GlobalDynamicParam>.Find(key)?.Val ?? defaultVal;
		}
	}
}
