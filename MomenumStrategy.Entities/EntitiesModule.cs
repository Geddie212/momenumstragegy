﻿using Microsoft.Practices.Unity;

namespace MomenumStrategy.Entities
{
	public class EntitiesModule 
	{
		public void Register(IUnityContainer unityContainer)
		{
			unityContainer.RegisterType<IStocksRepository, StocksRepository>(new ContainerControlledLifetimeManager());
			unityContainer.RegisterType<IGlobalDynamicParamRepository, GlobalDynamicParamRepository>(new ContainerControlledLifetimeManager());
			unityContainer.RegisterType<IMomenumStrategyUoW, MomenumStrategyUoW>(new PerThreadLifetimeManager());
		}
		
	}
}
