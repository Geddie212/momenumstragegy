﻿using Microsoft.Practices.ServiceLocation;
using System;

namespace MomenumStrategy.Entities
{
	public class CommitScope : IDisposable
	{
		private readonly IMomenumStrategyUoW _momenumStrategyUoW;
		private bool _rollback;
		public CommitScope()
		{
			_rollback = false;
			_momenumStrategyUoW = ServiceLocator.Current.GetInstance<IMomenumStrategyUoW>();
			_momenumStrategyUoW.Begin();
		}

		public void VoteRollback()
		{
			_rollback = true;
		}

		public void Flush()
		{
			_momenumStrategyUoW.Flush();
		}

		public void Dispose()
		{
			if (_rollback)
				_momenumStrategyUoW.Rollback();
			else
				_momenumStrategyUoW.Commit();
		}


		public static void ExecInScope(Action action)
		{
			using (var scope = new CommitScope())
			{
				try
				{
					action();
					scope.Flush();
				}
				catch
				{
					scope.VoteRollback();
					throw;
				}
			}
		}
	}
}
