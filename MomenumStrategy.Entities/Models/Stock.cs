using MomenumStrategy.Utils;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MomenumStrategy.Entities
{
	public partial class Stock : IFtsEntity
    {
        public int Id { get; set; }

        public bool IsDMark { get; set; }

        [Required]
        [StringLength(10)]
        public string Symbol { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
	    public double? RankValue { get; set; }
        public int Size { get; set; }
		public bool? HasNotGap { get; set; }
		public DateTime? GapAndSmaCaculationDate{ get; set; }
		public bool? IsStockAboveMa { get; set; }
		public decimal? AdditionalAmount { get; set; }
		[NotMapped]
	    public bool Active => !IsDMark;

		public DateTime? RankValueDate { get; set; }

		public bool Fit(string searchString)
	    {
		    if (string.IsNullOrEmpty(searchString))
		    {
			    return true;
		    }
		    return !string.IsNullOrWhiteSpace(Symbol) && Symbol.ToLower().Contains(searchString.ToLower()) ||
		           !string.IsNullOrWhiteSpace(Name) && Name.ToLower().Contains(searchString.ToLower());
	    }
    }
}
