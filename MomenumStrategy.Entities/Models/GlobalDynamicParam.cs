﻿namespace MomenumStrategy.Entities.Models
{
	public class GlobalDynamicParam
	{
		public string Id { get; set; }
		public string Val { get; set; }
	}
}
