﻿using MomenumStrategy.Applications.Serivces;
using MomenumStrategy.Entities;
using MomenumStrategy.Tests.Support;
using MomenumStrategy.Tests.Support.Fixtures;
using MomenumStrategy.Utils.Contituents;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.IntergrationTests.Stocks
{
	public class StocksServiceTests : BaseIntegrationTestClass<IStocksService>
	{
		[Test]
		public void should_add_new_stocks()
		{
			// arrange
			var list = new List<Constituent>
			{
				ConstituetFixture.InMemory.Create(),
				ConstituetFixture.InMemory.Create()
			};

			// act
			Sut.Actualize(list);

			// assert

			var actuals = StoreDbHelper<Stock>.All().Where(a => !a.IsDMark).ToList();
			Assert.That(actuals, Is.Not.Empty);
			Assert.That(actuals.Count, Is.EqualTo(2));
			Assert.That(actuals[0].Symbol, Is.EqualTo(list[0].Symbol));
			Assert.That(actuals[0].Name, Is.EqualTo(list[0].Name));
			Assert.That(actuals[1].Symbol, Is.EqualTo(list[1].Symbol));
			Assert.That(actuals[1].Name, Is.EqualTo(list[1].Name));
		}

		[Test]
		public void should_set_isDmark_if_symbol_is_not_in_actuals()
		{
			var stock = StockFixture.InDb.Create();
			Flush();

			Assert.That(StoreDbHelper<Stock>.All().Any(a=>!a.IsDMark && a.Symbol == stock.Symbol), Is.True);

			// arrange
			var list = new List<Constituent>
			{
				ConstituetFixture.InMemory.Create(),
				ConstituetFixture.InMemory.Create()
			};

			// act
			Sut.Actualize(list);

			// assert

			var actuals = StoreDbHelper<Stock>.All().Where(a => !a.IsDMark).ToList();
			Assert.That(actuals, Is.Not.Empty);
			Assert.That(actuals.Count, Is.EqualTo(2));
			Assert.That(actuals[0].Symbol, Is.EqualTo(list[0].Symbol));
			Assert.That(actuals[0].Name, Is.EqualTo(list[0].Name));
			Assert.That(actuals[1].Symbol, Is.EqualTo(list[1].Symbol));
			Assert.That(actuals[1].Name, Is.EqualTo(list[1].Name));
			Assert.That(StoreDbHelper<Stock>.All().Single(a=>a.Symbol == stock.Symbol).IsDMark, Is.True);
		}

		[Test]
		public void should_leave_it_actual_if_stock_besides_contituents()
		{
			var stock = StockFixture.InDb.Create();
			Flush();

			Assert.That(StoreDbHelper<Stock>.All().Any(a => !a.IsDMark && a.Symbol == stock.Symbol), Is.True);

			// arrange
			var list = new List<Constituent>
			{
				ConstituetFixture.InMemory.FromStock(stock).Create(),
				ConstituetFixture.InMemory.Create()
			};

			// act
			Sut.Actualize(list);

			// assert

			var actuals = StoreDbHelper<Stock>.All().Where(a => !a.IsDMark).ToList();
			Assert.That(actuals, Is.Not.Empty);
			Assert.That(actuals.Count, Is.EqualTo(2));
			Assert.That(actuals[0].Symbol, Is.EqualTo(list[0].Symbol));
			Assert.That(actuals[0].Name, Is.EqualTo(list[0].Name));
			Assert.That(actuals[1].Symbol, Is.EqualTo(list[1].Symbol));
			Assert.That(actuals[1].Name, Is.EqualTo(list[1].Name));
			Assert.That(StoreDbHelper<Stock>.All().Single(a => a.Symbol == stock.Symbol).IsDMark, Is.False);
		}

		[Test]
		public void should_set_symbol_actual_if_it_had_benn_marked_as_not_actual_before()
		{
			var stock = StockFixture.InDb.WithIsDMark(true).Create();
			Flush();

			Assert.That(StoreDbHelper<Stock>.All().Single(a =>a.Symbol == stock.Symbol).IsDMark, Is.True);

			// arrange
			var list = new List<Constituent>
			{
				ConstituetFixture.InMemory.FromStock(stock).Create(),
				ConstituetFixture.InMemory.Create()
			};

			// act
			Sut.Actualize(list);

			// assert

			var actuals = StoreDbHelper<Stock>.All().Where(a => !a.IsDMark).ToList();
			Assert.That(actuals, Is.Not.Empty);
			Assert.That(actuals.Count, Is.EqualTo(2));
			Assert.That(actuals[0].Symbol, Is.EqualTo(list[0].Symbol));
			Assert.That(actuals[0].Name, Is.EqualTo(list[0].Name));
			Assert.That(actuals[1].Symbol, Is.EqualTo(list[1].Symbol));
			Assert.That(actuals[1].Name, Is.EqualTo(list[1].Name));
			Assert.That(StoreDbHelper<Stock>.All().Single(a => a.Symbol == stock.Symbol).IsDMark, Is.False);
		}
	}
}
