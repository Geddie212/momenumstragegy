﻿using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using MomenumStrategy.Entities;
using MomenumStrategy.Tests.Support;
using MomenumStrategy.YF;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomenumStrategy.IntergrationTests
{
	[TestFixture]
	public class YahooDataProvider2Tests : BaseIntegrationTestClass<IYahooDataProvider2>
	{
		[Test]
		public void should_return_s_and_p_data()
		{
			var result = Sut.GetDailyPriceHistoryByNow("^GSPC", DateTime.Now.AddMonths(-2));
			Assert.That(result, Is.Not.Empty);
		}
		[Test]
		public void should_return_mmm_data()
		{
			var result = Sut.GetDailyPriceHistoryByNow("MMM", DateTime.Now.AddMonths(-2));
			Assert.That(result, Is.Not.Empty);
		}
		[Test]
		public void should_return_SP500TR_data()
		{
			var result = Sut.GetDailyPriceHistoryByNow("^SP500TR", DateTime.Now.AddMonths(-2));
			Assert.That(result, Is.Not.Empty);
		}


		[Test]
		public void should_return_the_data_for_the_most_of_the_symbols()
		{
			var dp = UnityContainer.Resolve<IStocksRepository>();
			var dic = new Dictionary<string, int>();
			dp.All().Where(a => a.Active).GroupBy(a => a.Symbol).ForEach(a =>
			  {
				  try
				  {
					  var result = Sut.GetDailyPriceHistoryByNow(a.Key, DateTime.Now.AddMonths(-2));
					  dic.Add(a.Key, result.Count());
				  }
				  catch (Exception exc)
				  {
					  dic.Add(a.Key, 0);
				  }
			  });

			Assert.That((Convert.ToDecimal(dic.Count(a => a.Value != 0)) / Convert.ToDecimal(dic.Count)), Is.GreaterThan(0.9));
		}
	}
}
