﻿using MomenumStrategy.YF;
using NUnit.Framework;
using System;
using System.Linq;

namespace MomenumStrategy.IntergrationTests
{
	[TestFixture]
	public class GoogleDataProviderTests
	{
		private GoogleDataProvider _sut;

		[SetUp]
		public void SetUp()
		{
			_sut = new GoogleDataProvider();
		}

		[Test]
		public void should_return_prices_for_about_a_year()
		{
			var s = _sut.GetDailyPriceHistoryByNow("MMM", DateTime.Now.AddYears(-1));
			Assert.That(s, Is.Not.Null);
			Assert.That(s.Any());
			Assert.That(s.Count() > 200);
		}
	}
}
