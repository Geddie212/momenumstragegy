﻿using MomenumStrategy.Tests.Support;
using MomenumStrategy.Utils;
using NUnit.Framework;

namespace MomenumStrategy.IntergrationTests
{
	[TestFixture]
	public class EmailServiceTests : BaseIntegrationTestClass<IEmailService>
	{
		[Test]
		public void should_send_email_without_errors()
		{
			var promise= Sut.SendEmailAsync("Test", "Test", true);
			promise.Wait();
			Assert.That(promise.Exception,Is.Null);
		}
	}
}
