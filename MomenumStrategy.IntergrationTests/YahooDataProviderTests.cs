﻿using MomenumStrategy.Tests.Support;
using MomenumStrategy.YF;
using NUnit.Framework;
using System;

namespace MomenumStrategy.IntergrationTests
{
	[TestFixture]
	public class YahooDataProviderTests : BaseIntegrationTestClass<IYahooDataProvider>
    {
		[Test]
		public void should_return_s_and_p_data()
		{
			var sut = new YahooDataProvider();
			var result = sut.GetDailyData(DateTime.Now.AddMonths(-2), DateTime.Now, "GSPC");
			Assert.That(result, Is.Not.Empty);
		}
		[Test]
		public void should_return_mmm_data()
		{
			var sut = new YahooDataProvider();
			var result = sut.GetDailyData(DateTime.Now.AddMonths(-2), DateTime.Now, "MMM");
			Assert.That(result, Is.Not.Empty);
		}
	}
}
