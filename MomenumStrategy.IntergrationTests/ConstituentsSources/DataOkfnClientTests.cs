﻿using MomenumStrategy.SAndPConstituentsSource;
using MomenumStrategy.Tests.Support;
using NUnit.Framework;

namespace MomenumStrategy.IntergrationTests.ConstituentsSources
{
	[TestFixture]
	public class DataOkfnClientTests : BaseIntegrationTestClass<IDataOkfnClient>
	{
		[Test]
		public void should_load_json()
		{
			var result = Sut.LoadAsJson();
			Assert.That(result, Is.Not.Null);
			Assert.That(result, Is.Not.Empty);
		}
	}
}
