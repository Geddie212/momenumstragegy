﻿using MomenumStrategy.Entities;
using MomenumStrategy.Tests.Support;
using MomenumStrategy.Tests.Support.Fixtures;
using NUnit.Framework;
using System.Linq;

namespace MomenumStrategy.IntergrationTests
{
	public class StocksRepositoryTests : BaseIntegrationTestClass<IStocksRepository>
	{
		[Test]
		public void should_load_all_stock_data()
		{
			var data = Sut.All();
			Assert.That(data, Is.Not.Empty);
		}

		[Test]
		public void should_create_stock()
		{
			//arrange
			var stock = StockFixture.InMemory.Create();

			//act
			Sut.Add(stock);
			Flush();

			//assert
			var loaded = StoreDbHelper<Stock>.All().SingleOrDefault(a => a.Symbol == stock.Symbol);
			Assert.That(loaded, Is.Not.Null);
		}

		[Test]
		public void should_update_rank_value()
		{
			//arrange
			var stock = StockFixture.InDb.Create();
			Flush();

			//act
			stock.RankValue = 1;
			Flush();

			//assert
			var loaded = StoreDbHelper<Stock>.All().SingleOrDefault(a => a.Symbol == stock.Symbol);
			Assert.That(loaded, Is.Not.Null);
			Assert.That(loaded.RankValue, Is.EqualTo(1));
		}
	}
}
