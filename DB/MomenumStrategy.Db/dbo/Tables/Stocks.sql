﻿CREATE TABLE [dbo].[Stocks]
(
	[Id] int IDENTITY (1, 1) NOT NULL
		  CONSTRAINT PK_Stocks_ID
		  PRIMARY KEY CLUSTERED WITH FILLFACTOR = 90,
    IsDMark bit not null
		  CONSTRAINT DF_Stocks_IsDMark
		  DEFAULT 0,
	Symbol nvarchar(10) not null,
	Name nvarchar(100) not null,
	Size int not null, 
    [RankValue] float NULL, 
    [HasNotGap] BIT NULL, 
    [IsStockAboveMa] BIT NULL,
	AdditionalAmount decimal(12,6) null,
	GapAndSmaCaculationDate datetime null
    --DataId UNIQUEIDENTIFIER NULL
		  --CONSTRAINT FK_CompanyFile_CompanyFileData_DataId
		  --FOREIGN KEY REFERENCES dbo.CompanyFileData(Id) NOT FOR REPLICATION, 
, 
    [RankValueDate] DATETIME NULL)
